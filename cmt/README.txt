################################
# Generating the documentation #
################################

You should refer to the automatically generated documentation.

The documentation is generated with sphinx. If this is not installed then you can install it with the command:

pip install --user sphinx

or 

easy_install --user sphinx

(note you should set up your path to point at this installation directory e.g. in your .bashrc
export PATH=${HOME}/.local/bin:${PATH}
)

Setup the package with:

cd cmt
cmt config && source setup.sh

and finally make the doucmentation.

cd ../doc
./makeSphinxDoc.sh

It can be found in doc/_build/html/index.html.

###########
# License #
###########

Not for public release.

