""" plot package provides tools for generating, drawing and saving histograms.

"""

#import the modules in this sub package so that they can be used directly e.g.
# import package
# package.module.function()
# rather than having to do:
# import package
# import package.module
# 
# package.module.function()

from . import drawTools
from . import drawOptions
from . import io
from . import palette
from . import style
from . import table

#style.setDefaultStyle()
