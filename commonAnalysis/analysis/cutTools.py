import ROOT
from ..tools import ntuple
import copy
import StringIO
from ..plot import table
import pprint
import itertools

###############################################################################

class BaseSelection:
    def __init__(self, name, title):
        self.name = name
        self.cutFlow = CutFlow(name,"Cut flow for "+title)
        
    #def applySelection(self, event):
    #    return False
    
    def getCutFlow(self):
        return self.cutFlow
    
###############################################################################

class CutFlow:
    
    outputTreeName = "CutFlow"
    
    def __init__(self, name, title, registerAllEvents=True):
        self.name = name
        self.title = title
        self.cuts = []
        self.cumulativeCounters = []
        self.individualCounters = []
        if registerAllEvents:
            self.cutAllEvents = self.registerCut("all_"+name, "all input events for this cut flow ("+self.title+")")
            self.cutAllEvents.setPassed(True)
        self.setEventType(None)
        return
    
    def registerCut(self, name, title, ignored=False):
        cut = Cut(name, title)
        cut.setIgnored(ignored)
        cumCount = Counter(name,title)
        indCount = Counter(name,title)
        self.cuts.append(cut)
        self.cumulativeCounters.append(cumCount)
        self.individualCounters.append(indCount)
        return cut
    
    def setEventType(self, eventType):
        if eventType is None:
            eventType = "other"
        self.eventType = eventType
        return
    
    def _getEventType(self):
        return self.eventType
    
    def hasPassedAllCuts(self):
        return all((c.getPassed() for c in self.cuts))
    
    def fill(self, pot, eventWeight, reset=True):
        eventType = self._getEventType()
        passedCumulative = True
        for i,cut in enumerate(self.cuts):
            passed = cut.getPassed()
            ignored = cut.isIgnored()
            if passed:
                self.individualCounters[i].fill(eventType, pot,eventWeight)
            passedCumulative = (passed or ignored) and passedCumulative 
            if passed and passedCumulative:
                self.cumulativeCounters[i].fill(eventType, pot,eventWeight)
        if reset:
            self.resetCuts()
        return 
    
    def resetCuts(self):
        for cut in self.cuts:
            cut.setPassed(False)
        self.cutAllEvents.setPassed(True)
        return
    
    def writeToFile(self, outputFile):
        outputFile.cd()
        #create an output tree
        treeName = CutFlow.outputTreeName
        tree = outputFile.Get(treeName)
        if not tree:
            tree = ROOT.TTree(treeName,self.title)
        outName = ntuple.BranchObject(ROOT.std.string, "cutFlowName", tree)
        outTitle = ntuple.BranchObject(ROOT.std.string, "cutFlowTitle", tree)
        cutName = ntuple.BranchObject(ROOT.std.string, "cutName", tree)
        cutTitle = ntuple.BranchObject(ROOT.std.string, "cutTitle", tree)
        cutNumber = ntuple.BranchPrimative("cutNumber", tree)
        cutIgnored = ntuple.BranchPrimative("cutIgnored", tree)
        numCuts = ntuple.BranchPrimative("numCuts", tree)
        eventTypeBranch = ntuple.BranchObject(ROOT.std.string, "eventType", tree)
        cum_rawCount = ntuple.BranchPrimative("cum_rawCount", tree)
        cum_pot = ntuple.BranchPrimative("cum_pot", tree)
        cum_eventWeight = ntuple.BranchPrimative("cum_eventWeight", tree)
        cum_potTimesEventWeight = ntuple.BranchPrimative("cum_potTimesEventWeight", tree)
        ind_rawCount = ntuple.BranchPrimative("ind_rawCount", tree)
        ind_pot = ntuple.BranchPrimative("ind_pot", tree)
        ind_eventWeight = ntuple.BranchPrimative("ind_eventWeight", tree)
        ind_potTimesEventWeight = ntuple.BranchPrimative("ind_potTimesEventWeight", tree)
        #set values
        outName.copyValue(ROOT.std.string(self.name))
        outTitle.copyValue(ROOT.std.string(self.title))
        numCuts.setValue(len(self.cuts))
        for i,cut in enumerate(self.cuts):
            #information about the cut
            cutName.copyValue(cut.name)
            cutTitle.copyValue(cut.title)
            cutNumber.setValue(float(i))
            cutIgnored.setValue(float(cut.isIgnored()))
            listOfEventTypes = set(self.cumulativeCounters[i].counters.keys()+self.individualCounters[i].counters.keys())
            for eventType in listOfEventTypes:
                eventTypeBranch.copyValue(str(eventType))
                #reset values to zero
                cum_rawCount.setValue(0.0)
                cum_pot.setValue(0.0)
                cum_eventWeight.setValue(0.0)
                cum_potTimesEventWeight.setValue(0.0)
                ind_rawCount.setValue(0.0)
                ind_pot.setValue(0.0)
                ind_eventWeight.setValue(0.0)
                ind_potTimesEventWeight.setValue(0.0)
                #information about cumulative count
                cumCount = self.cumulativeCounters[i]
                if eventType in cumCount.counters:
                    c = cumCount.counters[eventType]
                    cum_rawCount.setValue( c.rawCount )
                    cum_pot.setValue( c.pot )
                    cum_eventWeight.setValue( c.eventWeight )
                    cum_potTimesEventWeight.setValue( c.potTimesEventWeight )
                #information about the individual count
                indCount = self.individualCounters[i]
                if eventType in indCount.counters:
                    c = indCount.counters[eventType]
                    ind_rawCount.setValue( c.rawCount )
                    ind_pot.setValue( c.pot )
                    ind_eventWeight.setValue( c.eventWeight )
                    ind_potTimesEventWeight.setValue( c.potTimesEventWeight )
                #fill the tree
                tree.Fill()
        #save output
        tree.Write()
        return
    
    @staticmethod
    def readFromFile(inputFileHandle):
        #retrieve tree
        treeName = CutFlow.outputTreeName
        tree = inputFileHandle.Get(treeName)
        result = CutFlow("","", registerAllEvents=False)
        isFirstEvent = True
        for entry in tree:
            result.name = str(entry.cutFlowName)
            result.title = str(entry.cutFlowTitle)
            if isFirstEvent:
                nCuts = int(entry.numCuts)
                result.cuts = [None]*nCuts
                result.cumulativeCounters = [None]*nCuts
                result.individualCounters = [None]*nCuts
                isFirstEvent = False
            #load cut
            cut = Cut(str(entry.cutName),str(entry.cutTitle))
            try:
                cut.setIgnored( not entry.cutIgnored==0.0 )
            except AttributeError:
                cut.setIgnored(False)
            cutNum = int(entry.cutNumber)
            result.cuts[cutNum] = cut
            eventType = str(entry.eventType)
            #load cumulative counters
            counterUnit = CounterUnit(str(entry.cutName),str(entry.cutTitle))
            counterUnit.rawCount = int(entry.cum_rawCount)
            counterUnit.pot = float(entry.cum_pot)
            counterUnit.eventWeight = float(entry.cum_eventWeight)
            counterUnit.potTimesEventWeight = float(entry.cum_potTimesEventWeight)
            counter = Counter(str(entry.cutName),str(entry.cutTitle))
            counter.insert(eventType,counterUnit)
            if result.cumulativeCounters[cutNum] is None:
                result.cumulativeCounters[cutNum] = counter 
            else:
                result.cumulativeCounters[cutNum] += counter
            #load cumulative counters
            counterUnit = CounterUnit(str(entry.cutName),str(entry.cutTitle))
            counterUnit.rawCount = int(entry.ind_rawCount)
            counterUnit.pot = float(entry.ind_pot)
            counterUnit.eventWeight = float(entry.ind_eventWeight)
            counterUnit.potTimesEventWeight = float(entry.ind_potTimesEventWeight)
            counter = Counter(str(entry.cutName),str(entry.cutTitle))
            counter.insert(eventType,counterUnit)
            if result.individualCounters[cutNum] is None:
                result.individualCounters[cutNum] = counter 
            else:
                result.individualCounters[cutNum] += counter
        return result
    
    @staticmethod
    def merge(first,second):
        '''Merge cut flows from two separate processing steps. e.g. a skim step then an ntuple step.'''
        result = first.clone()
        result.cuts.extend(copy.deepcopy(second.cuts))
        result.cumulativeCounters.extend(copy.deepcopy(second.cumulativeCounters))
        result.individualCounters.extend(copy.deepcopy(second.individualCounters))
        return result

    @staticmethod
    def link(first,second):
        '''Link cut flows from two separate processing steps. e.g. a skim step then an ntuple step.'''
        result = first.clone()
        result.cutAllEvents = first.cutAllEvents
        result.cuts = list(first.cuts) #track the original set of cuts
        result.cuts.extend(second.cuts[1:]) #skip first cut as this is the "all events" cut
        result.cumulativeCounters.extend(second.cumulativeCounters[1:])
        result.individualCounters.extend(second.individualCounters[1:])
        return result
    
    def add(self, rhs):
        if not len(rhs)==len(self):
            raise Exception("Error attempt to add cut flows with differing number of cuts")
        for i in xrange(len(self)):
            self.cumulativeCounters[i] += rhs.cumulativeCounters[i]
            self.individualCounters[i] += rhs.individualCounters[i]
        return
    
    def clone(self):
        return copy.deepcopy(self)
    
    def prettyPrint(self):
        print str(self)
        for c in itertools.chain(self.cumulativeCounters):
            print "Cumulative",str(c)
        return
    
    def __len__(self):
        return len(self.cuts)
    
    def __str__(self):
        info = [self.name,self.title,str(len(self))]
        s = "CutFlow("+",".join(info)+")"
        return s
    
    def getPotWeight(self, eventType):
        result = 1.0
        if not "data" in eventType:
            dataPot = self._getTotalDataPot()
            mcPot = self._getTotalMcPot()
            #print dataPot,mcPot
            if dataPot > 0.0 and mcPot > 0.0:
                result = dataPot / mcPot
        #print eventType,result
        return result
    
    def _getTotalDataPot(self):
        return self._getTotalPot(True)

    def _getTotalMcPot(self):
        return self._getTotalPot(False)
        
    def _getTotalPot(self, data):
        pot = 0.0
        firstCounter = self.cumulativeCounters[0]
        for et,cu in firstCounter.counters.iteritems():
            #print "getTotalPot",et,cu.pot
            isDataEt = "data" in et
            if data and isDataEt:
                pot += cu.pot
            elif (not data) and (not isDataEt):
                pot += cu.pot
        return pot

###############################################################################

class CutFlowTableFormat:
    format_efficiency = "format_efficiency"
    
    def __init__(self, signalList=None, showEfficiency=False, mergeBackground=False):
        if signalList is None:
            signalList = []
        self.setSignal(*signalList)
        self.mergeBackground = mergeBackground
        self.showEfficiency = showEfficiency
    
    def includeColumn(self, et):
        r = True
        if self.mergeBackground:
            r = self.isSignal(et)
        return r
    
    def isSignal(self, et):
        return et in self.signalList
        
    def setSignal(self, *args):
        self.signalList = args
        return
    
    def doShowTotalBackground(self):
        return self.mergeBackground
    
    def doShowEfficiency(self):
        return self.showEfficiency
    
    def doShowPurity(self):
        return self.showEfficiency    

###############################################################################

class CutFlowPainter:
    
    def __init__(self, cutFlow, format=None):
        self.cutFlow = cutFlow
        self.signal = None
        if format is None:
            format = CutFlowTableFormat()
        self.format = format
        return
    
    def makeTable(self):
        cutFlow = self.cutFlow
        #cutFlow.cumulativeCounters = cutFlow.individualCounters
        listOfEventTypes = []
        for counter in self.cutFlow.cumulativeCounters:
            listOfEventTypes.extend(counter.counters.keys())
        listOfEventTypes = sorted(set(listOfEventTypes))
        headerRow = ["Cut"]+[et for et in listOfEventTypes if self.format.includeColumn(et)]
        if self.format.doShowTotalBackground():
                headerRow.append("Background")
        if self.format.doShowEfficiency():
                headerRow.append("Eff")
        if self.format.doShowPurity():
                headerRow.append("Purity")
        tbl = table.Table(cutFlow.name)
        tbl.setFloatingPointFormatString("{:.0f}")
        tbl.setHeaderRow(headerRow)
        initialSignalTotal = 0.0
        for iCut in xrange(len(cutFlow.cuts)):
            cut = cutFlow.cuts[iCut]
            counter = cutFlow.cumulativeCounters[iCut]
            cutText = cut.getTitle()
            #cutText = cut.getName()
            row = [cutText]
            signalTotal = 0.0
            backgroundTotal = 0.0
            for et in listOfEventTypes:
                val = 0.0
                if et in counter.counters:
                    cu = counter.counters[et]
                    #val = cu.rawCount
                    potWeight = cutFlow.getPotWeight(et)
                    val= cu.eventWeight*potWeight
                else:
                    val = 0.0
                if self.format.includeColumn(et):
                    row.append(val)
                if self.format.isSignal(et):
                    signalTotal += val
                else:
                    backgroundTotal += val
            #determine starting signal total for efficiency calculation
            if initialSignalTotal==0.0:
                initialSignalTotal = signalTotal
            #add background total column
            if self.format.doShowTotalBackground():
                row.append(backgroundTotal)
            #add efficiency column
            if self.format.doShowEfficiency():
                try:
                    eff = float(signalTotal)/float(initialSignalTotal)
                except ZeroDivisionError:
                    eff = 0.0
                row.append("%.2f"%eff)
            #add purity column
            if self.format.doShowPurity():
                try:
                    purity = float(signalTotal) / float(signalTotal+backgroundTotal)
                except ZeroDivisionError:
                    purity = 1.0
                row.append("%.2f"%purity)
            tbl.addRow(row)
            #cumCount = cutFlow.cumulativeCounters[iCut]
            #indCount = cutFlow.individualCounters[iCut]
        return tbl
    
    def plainTableString(self):
        tbl = self.makeTable()
        return tbl.getAsciiTable()
    
    def printTable(self):
        print self.plainTableString()
        return

###############################################################################

class CutFlowLoader:
    '''Provides an simple way to load and merge paralell CutFlows from multiple files.'''
    def __init__(self, name, title):
        self.cutFlow = None
    
    def load(self, inputFileName):
        inFile = ROOT.TFile(inputFileName)
        tree = inFile.Get(CutFlow.outputTreeName)
        if tree:
            fileCutFlow = CutFlow.readFromFile(inFile)
            if not self.cutFlow:
                self.cutFlow = fileCutFlow
            else:
                self.cutFlow.add(fileCutFlow)
        return
    
    def loadDataset(self, dataset):
        for fileName in dataset.iterFiles():
            self.load(fileName)
        return
    
    def getCutFlow(self):
        return self.cutFlow
    
###############################################################################
    
class Cut:
    def __init__(self, name, title):
        self.name = name
        self.title = title
        self.passed = False
        self.ignored = False
        return
    
    def setPassed(self, passed=True):
        self.passed = passed
        
    def getPassed(self):
        return self.passed
    
    def setIgnored(self, ignored):
        self.ignored = ignored
        
    def isIgnored(self):
        return self.ignored
    
    def getName(self):
        return self.name
    
    def getTitle(self):
        return self.title
    
    def __str__(self):
        info = ",".join([self.name,self.title])
        s = "Cut("+info+")"
        return s
    
###############################################################################

class Counter:
    def __init__(self, name, title):
        self.name = name
        self.title = title
        self.counters = dict()
        #force there to be at least one entry in to dictionary (for IO purposes)
        self.counters["other"] = CounterUnit(self.name,self.title)
        
    def __iadd__(self, rhs):
        for rkey in rhs.counters.iterkeys():
            if rkey in self.counters:
                self.counters[rkey] += rhs.counters[rkey]
            else:
                self.counters[rkey] = rhs.counters[rkey]
        return self
        
    def fill(self, eventType, pot, eventWeight):
        try:
            counter = self.counters[eventType]
        except KeyError:
            counter = CounterUnit(self.name,self.title)
            self.counters[eventType] = counter
        counter.fill(pot,eventWeight)
        return
    
    def insert(self, eventType, counterUnit):
        self.counters[eventType] = counterUnit
        return
    
    def __str__(self):
        info = ",".join([self.name,self.title])
        s = "Counter("+info+")"
        return s

###############################################################################
    
class CounterUnit:
    def __init__(self, name, title):
        self.name = name
        self.title = title
        self.rawCount = 0
        self.pot = 0.0
        self.eventWeight = 0.0
        self.potTimesEventWeight = 1.0
        
    def __iadd__(self, rhs):
        self.rawCount += rhs.rawCount
        self.pot += rhs.pot
        self.eventWeight += rhs.eventWeight
        self.potTimesEventWeight += rhs.potTimesEventWeight
        return self
        
    def fill(self, pot, eventWeight):
        self.rawCount += 1
        self.pot += pot
        self.eventWeight += eventWeight
        self.potTimesEventWeight += pot*eventWeight
        
###############################################################################

def _testWriteCutFlow():
    import random
    cutFlow = CutFlow("test","test")
    cut1 = cutFlow.registerCut("firstCut","a dummy first cut")
    cut2 = cutFlow.registerCut("secondCut","a dummy second cut")
    cut3 = cutFlow.registerCut("thirdCut","a dummy third cut")
    for eventTypes in (["data"],["e-", "mu-","proton",None]):
        nEvents = 2
        if "data" in eventTypes:
            pot = 222.22
            eventWeight = 1.0
            nEvents = 1000
        else:
            pot = 222.22*2.0
            eventWeight = 2.0
            nEvents = 10000
        for i in xrange(nEvents):
            et = random.choice(eventTypes)
            cutFlow.setEventType(et)
            r = random.randint(0,3)
            if r >= 1:
                cut1.setPassed()
            if r >= 2:
                cut2.setPassed()
            if r >= 3:
                cut3.setPassed()
            cutFlow.fill(pot,eventWeight)
    outFile = ROOT.TFile("test.root","recreate")
    cutFlow.writeToFile(outFile)
    return

###############################################################################

def _testReadCutFlow():
    inFile = ROOT.TFile("test.root","read")
    cutFlow = CutFlow.readFromFile(inFile)
    paint = CutFlowPainter(cutFlow)
    paint.printTable()
    return

###############################################################################

def main():
    _testWriteCutFlow()
    _testReadCutFlow()
    return

###############################################################################

if __name__ == "__main__":
    main()