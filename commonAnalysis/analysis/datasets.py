import glob
import os
import itertools
import pprint

from ..tools import hosts
import StringIO

###############################################################################

class Dataset:
    def __init__(self, name, *filePatterns):
        self.name = name
        self._filePatterns = filePatterns
        self._hasLoaded = False
        
    def _load(self):
        if not self._hasLoaded:
            self._hasLoaded = True
            self._fileList = []
            for pattern in self._filePatterns:
                matchingFiles = self._findMatchingFiles(pattern)
                self._fileList.extend(matchingFiles)
            self._fileList.sort()
            self._sanityChecks()
        
    @staticmethod
    def merge(name, datasets):
        filePatterns = []
        for d in datasets:
            filePatterns.extend( d._filePatterns )
        #print filePatterns
        return Dataset(name,*filePatterns)
    
    def getName(self):
        return self.name
    
    def getFirstFile(self):
        self._load()
        if len(self)==0:
            raise Exception("error: no matching files found",self._filePatterns)
        return iter(self).next()
    
    def getFileList(self):
        self._load()
        return self._fileList
    
    def getTotalFileSize(self):
        '''
        :returns: sum of file sizes in bytes.
        :rtype: int or long int
        '''
        self._load()
        totalSize = 0L
        for fileName in self.iterFiles():
            try:
                sizeInBytes = os.path.getsize(fileName)
                totalSize += sizeInBytes
            except os.error:
                pass
        result = totalSize
        #convertToMB = 9.53674e-7
        #result = float(totalSize)*convertToMB
        return result
    
    def getChunk(self, nChunk, totalChunks):
        '''Divides the input dataset into totalChunks approximately equal sized chunks.
        
        This method is useful for breaking datasets up for batch jobs.
        For example, consider the dataset file01.root, file02.root ... file10.root. 
        If dataset is broken up into 3 chunks the chunks will correspond to:
        
        chunk0 : 01, 02, 03, 04,
        chunk1 : 05, 06, 07, 
        chunk2 : 08, 09, 10, 
        
        Hence, getChunk(1,3) returns files 5,6,7.
        
        Returns the file list corresponding to chunk number nChunks.'''
        self._load()
        #create a list of lists with N_totalChunks entries. 
        allChunks = [list() for i in xrange(totalChunks)]
        #evenly distribute empty slots (i.e. value None in list) between each chunk
        # with total number of slots == total number of files. 
        loopChunks = itertools.cycle(allChunks)
        for fileNum in xrange(len(self._fileList)):
            loopChunks.next().append(None)
        #now fill the empty slots with file names.
        iterFiles = iter(self._fileList)
        for chunkList in allChunks:
            for i in xrange(len(chunkList)):
                chunkList[i] = iterFiles.next()
        fileList = allChunks[nChunk]
        name = self.getName() + "."+str(nChunk)+"of"+str(totalChunks)
        result = Dataset(name,*fileList)
        return result
        
    def _findMatchingFiles(self, pattern):
        pattern = os.path.expanduser(pattern)
        pattern = os.path.expandvars(pattern)
        result = glob.glob(pattern)
        result.sort() #ensure iterations are always in the same order.
        return result
    
    def _sanityChecks(self):
        #check for duplicates
        fileSet = set(self._fileList)
        if not len(self._fileList)==len(fileSet):
            raise Exception("input file patterns contain duplicate files.",)
    
    def __len__(self):
        self._load()
        return len(self._fileList)
    
    def __iter__(self):
        return self.iterFiles()
    
    def iterFiles(self):
        self._load()
        for fileName in self._fileList:
            yield fileName
            
    def __str__(self):
        self._load()
        return "Dataset("+self.getName()+", "+str(len(self))+" files)"

    def infoString(self):
        sio = StringIO.StringIO()
        print >>sio,"Dataset(",self.getName(),")"
        indent = 4*" "
        print >>sio,indent,len(self._filePatterns),"patterns"
        pprint.pprint(self._filePatterns,sio,indent=8)
        print >>sio,indent,len(self),"matched files."
        pprint.pprint(self._fileList,sio,indent=8)
        return sio.getvalue()

###############################################################################

class DatasetDefs:
    #the root folder that stores files
    if hosts.isHost(hosts.daveLaptop):
        dataRoot = "/home/dave/t2k/data/nd280"
    elif hosts.isHost(hosts.cscDesktop):
        dataRoot = "/storage/epp2/t2k/data/nd280"
    elif hosts.isHost(hosts.warwickCluster):
        dataRoot = "/data/t2k/data"
    elif hosts.isHost(hosts.andyLaptop):
        dataRoot = "~/T2K/data"
    else:
        raise Exception("DatasetDefs cannot determine dataRoot.")
    
    #rdp production 5 datasets
    rdp_production5B = Dataset("rdp_production5B", dataRoot+"/production005/B/rdp/ND280/0000[0-9]000_0000[0-9]999/anal/oa_nd_spl_*.root")
    rdp_production5F = Dataset("rdp_production5F", dataRoot+"/production005/F/rdp/ND280/0000[0-9]000_0000[0-9]999/anal/oa_nd_spl_*.root")
    
    #mcp production 5C datasets
    mcp_production5C_corsika_2010_11_water = Dataset("mcp_production5C_corsika_2010_11_water",
                                                          dataRoot+"/production005/C/mcp/cosmic/2010-11-water/corsikanew/allcosmic/anal/oa_cs_mu_*.root")
    mcp_production5C_genie_magnet_2010_02_water = Dataset("mcp_production5C_genie_magnet_2010_02_water",
                                                          dataRoot+"/production005/C/mcp/genie/2010-02-water/magnet/beama/anal/oa_gn_beam_*.root")
    mcp_production5C_genie_magnet_2010_11_air = Dataset("mcp_production5C_genie_magnet_2010_11_air",
                                                          dataRoot+"/production005/C/mcp/genie/2010-11-air/magnet/beamb/anal/oa_gn_beam_*.root")
    mcp_production5C_genie_magnet_2010_11_water = Dataset("mcp_production5C_genie_magnet_2010_11_water",
                                                          dataRoot+"/production005/C/mcp/genie/2010-11-water/magnet/beamb/anal/oa_gn_beam_*.root")
    mcp_production5C_neut_magnet_2010_02_water = Dataset("mcp_production5C_neut_magnet_2010_02_water",
                                                          dataRoot+"/production005/C/mcp/neut/2010-02-water/magnet/beama/anal/oa_nt_beam_*.root")
    mcp_production5C_neut_magnet_2010_11_air = Dataset("mcp_production5C_neut_magnet_2010_11_air",
                                                          dataRoot+"/production005/C/mcp/neut/2010-11-air/magnet/beamb/anal/oa_nt_beam_*.root")
    mcp_production5C_neut_magnet_2010_11_water = Dataset("mcp_production5C_neut_magnet_2010_11_water",
                                                          dataRoot+"/production005/C/mcp/neut/2010-11-water/magnet/beamb/anal/oa_nt_beam_*.root")
    
    #mcp production 5E datasets
    mcp_production5E_corsika_2010_11_water = Dataset("mcp_production5E_corsika_2010_11_water",
                                                          dataRoot+"/production005/E/mcp/cosmic/2010-11-water/corsikanew/allcosmic/anal/oa_cs_mu_*.root")
    mcp_production5E_genie_magnet_2010_11_air_beamb = Dataset("mcp_production5E_genie_magnet_2010_11_air_beamb",
                                                          dataRoot+"/production005/E/mcp/genie/2010-11-air/magnet/beamb/anal/oa_gn_beam_*.root")
    mcp_production5E_genie_magnet_2010_11_air_beamc = Dataset("mcp_production5E_genie_magnet_2010_11_air_beamc",
                                                          dataRoot+"/production005/E/mcp/genie/2010-11-air/magnet/beamc/anal/oa_gn_beam_*.root")
    mcp_production5E_genie_magnet_2010_11_water_beamb = Dataset("mcp_production5E_genie_magnet_2010_11_water_beamb",
                                                          dataRoot+"/production005/E/mcp/genie/2010-11-water/magnet/beamb/anal/oa_gn_beam_*.root")

    #mcp production 5F datasets
    mcp_production5F_genie_magnet_2010_11_air_beamb = Dataset("mcp_production5F_genie_magnet_2010_11_air_beamb",
                                                          dataRoot+"/production005/F/mcp/genie/2010-11-air/magnet/beamb/anal/oa_gn_beam_*.root")    
    mcp_production5F_genie_magnet_2010_11_water_beamb = Dataset("mcp_production5F_genie_magnet_2010_11_water_beamb",
                                                          dataRoot+"/production005/F/mcp/genie/2010-11-water/magnet/beamb/anal/oa_gn_beam_*.root")
    mcp_production5F_genie_magnet_2010_11_air_beamc = Dataset("mcp_production5F_genie_magnet_2010_11_air_beamc",
                                                          dataRoot+"/production005/F/mcp/genie/2010-11-air/magnet/beamc/anal/oa_gn_beam_*.root")    
    mcp_production5F_genie_magnet_2010_11_water_beamc = Dataset("mcp_production5F_genie_magnet_2010_11_water_beamc",
                                                          dataRoot+"/production005/F/mcp/genie/2010-11-water/magnet/beamc/anal/oa_gn_beam_*.root")

    mcp_production5F_neut_magnet_2010_11_air_beamb = Dataset("mcp_production5F_neut_magnet_2010_11_air_beamb",
                                                          dataRoot+"/production005/F/mcp/neut/2010-11-air/magnet/beamb/anal/oa_*_beam_*.root")    
    mcp_production5F_neut_magnet_2010_11_water_beamb = Dataset("mcp_production5F_neut_magnet_2010_11_water_beamb",
                                                          dataRoot+"/production005/F/mcp/neut/2010-11-water/magnet/beamb/anal/oa_*_beam_*.root")
    mcp_production5F_neut_magnet_2010_11_air_beamc = Dataset("mcp_production5F_neut_magnet_2010_11_air_beamc",
                                                          dataRoot+"/production005/F/mcp/neut/2010-11-air/magnet/beamc/anal/oa_*_beam_*.root")    
    mcp_production5F_neut_magnet_2010_11_water_beamc = Dataset("mcp_production5F_neut_magnet_2010_11_water_beamc",
                                                          dataRoot+"/production005/F/mcp/neut/2010-11-water/magnet/beamc/anal/oa_*_beam_*.root")

    #mcp production 6 validation datasets
    mcp_production6validation_genie_magnet_2010_11_water_run2 = Dataset("mcp_production6validation_genie_magnet_2010_11_water_run2",
                                         dataRoot+"/production006/A/mcp/verify/v11r21/genie/2010-11-water/magnet/run2/anal/oa_*.root"
                                         )
    mcp_production6validation_genie_magnet_2010_11_water_run4 = Dataset("mcp_production6validation_genie_magnet_2010_11_water_run4",
                                         dataRoot+"/production006/A/mcp/verify/v11r21/genie/2010-11-water/magnet/run4/anal/oa_*.root"
                                         )
    
    mcp_production6validation_genie_magnet_2010_11_water_run2 = Dataset("mcp_production6validation_genie_magnet_2010_11_air_run2",
                                         dataRoot+"/production006/A/mcp/verify/v11r21/genie/2010-11-air/magnet/run2/anal/oa_*.root"
                                         )
    mcp_production6validation_genie_magnet_2010_11_water_run3 = Dataset("mcp_production6validation_genie_magnet_2010_11_water_run3",
                                         dataRoot+"/production006/A/mcp/verify/v11r21/genie/2010-11-air/magnet/run3/anal/oa_*.root"
                                         )

    #rdp production 6 validation
    rdp_production6validation = Dataset("rdp_production6validation", dataRoot+"/production006/A/rdp/verify/v11r21/ND280/0000[0-9]000_0000[0-9]999/anal/oa_nd_spl_*.root")

    @staticmethod
    def getDataset(name):
        return getattr(DatasetDefs,name)
    
    @staticmethod
    def getValidDatasets():
        return list(DatasetDefs._allValidDatasets)
    
    @staticmethod
    def getValidDatasetNames():
        validDatasets = DatasetDefs.getValidDatasets()
        names = [d.getName() for d in validDatasets]
        return names
    
    @staticmethod
    def getValidMCDatasets():
        return list(DatasetDefs._allValidMCDatasets)
    
    @staticmethod
    def getValidMCDatasetNames():
        validDatasets = DatasetDefs.getValidMCDatasets()
        names = [d.getName() for d in validDatasets]
        return names

    _allValidMCDatasets = [
                         mcp_production5C_corsika_2010_11_water,
                         mcp_production5C_genie_magnet_2010_11_air,
                         mcp_production5C_genie_magnet_2010_11_water,
                         mcp_production5C_neut_magnet_2010_02_water,
                         mcp_production5C_neut_magnet_2010_11_air,
                         mcp_production5C_neut_magnet_2010_11_water,
                         
                         mcp_production5E_corsika_2010_11_water,
                         mcp_production5E_genie_magnet_2010_11_air_beamb,
                         mcp_production5E_genie_magnet_2010_11_air_beamc,
                         mcp_production5E_genie_magnet_2010_11_water_beamb,
                         
                         mcp_production5F_genie_magnet_2010_11_air_beamb,
                         mcp_production5F_genie_magnet_2010_11_water_beamb,
                         mcp_production5F_genie_magnet_2010_11_air_beamc,
                         mcp_production5F_genie_magnet_2010_11_water_beamc,
                         
                         mcp_production5F_neut_magnet_2010_11_air_beamb,
                         mcp_production5F_neut_magnet_2010_11_water_beamb,
                         mcp_production5F_neut_magnet_2010_11_air_beamc,
                         mcp_production5F_neut_magnet_2010_11_water_beamc,
                         
                         mcp_production6validation_genie_magnet_2010_11_water_run2,
                         mcp_production6validation_genie_magnet_2010_11_water_run4,
                         mcp_production6validation_genie_magnet_2010_11_water_run2,
                         mcp_production6validation_genie_magnet_2010_11_water_run3,
                         ]

    _allValidDatasets = [rdp_production5B,
                         rdp_production5F,
                         rdp_production6validation,
                         ] + _allValidMCDatasets
    
    

###############################################################################

def _checkDatasets(warning=False):
    failed = False
    emptysets = ["",]
    for d in DatasetDefs.getValidDatasets():
        print str(d)
        if len(d)==0:
            failed = True
            emptysets.append(d.name)
            
    if failed:
        msg = "datasets with no input files" + "\n         ".join(emptysets)
        if not warning:
            raise Exception("ERROR : " + msg)
        else:
            print "WARNING: " + msg

###############################################################################

def main():
    _checkDatasets(warning=True)
    return

if __name__ == "__main__":
    main()
    


