import ROOT 

###############################################################################

class TrajTree:
    def __init__(self,listOfTrajectories):
        self.setup(listOfTrajectories)
        return
    def setup(self,listOfTrajectories):
        self.nodes = dict()
        self.firstTraj = None
        for traj in listOfTrajectories:
            #id = traj.ID
            print traj,traj.PrimaryID,traj.ParentID
            import pprint
            pprint.pprint(dir(traj))
            print traj.ID
            id = traj.ID
            if id in self.nodes:
                raise Exception("error duplicated traj id",id)
            self.nodes[id] = TrajNode(traj)
            if self.firstTraj is None:
                 self.firstTraj = traj
        #set children
        for node in self.nodes.itervalues():
            parent = node.getParent()
            if not parent == 0:
                self.nodes[parent].addChild(node.getId())
        return
    def iterTraj(self):
        return self.nodes.itervalues()
    def getFirst(self):
        return self.firstTraj
    def getChildren(self, traj):
        children = []
        id = traj.ID
        if id in self.nodes:
            for childId in self.nodes[id].iterChildren():
                children.append(self.nodes[childId].getTraj())
        else:
            raise Exception("TrajTree does not know about this trajectory",traj,id)
        return children
    def printTraj(self,traj,prefix=""):
        pdg = ROOT.TDatabasePDG()
        pdgPart = pdg.GetParticle(traj.PDG)
        print prefix,"id =",traj.ID,", pdg =",pdgPart.GetName()," nChildren =",len(self.getChildren(traj)),"mom=","%.0fMeV"%traj.InitMomentum.P()
    
    def trajectoryShowers(self):
        return

###############################################################################

class TrajNode:
    def __init__(self,traj):
        self.traj = traj
        self.children = []
    def getTraj(self):
        return self.traj
    def getParent(self):
        return self.traj.ParentID
    def getId(self):
        return self.traj.ID
    def iterChildren(self):
        return iter(self.children)
    def addChild(self,child):
        self.children.append(child)

###############################################################################

def main():
    return

if __name__ == "__main__":
    main()