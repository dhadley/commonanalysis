
import ROOT

defaultInputTrees = ["HeaderDir/BasicHeader",
                     "HeaderDir/BasicDataQuality",
                     "HeaderDir/BeamSummaryData",
                     #"HeaderDir/GeometrySummary",
                     #"ReconDir/P0D",
                     #"ReconDir/P0DECal",
                     "ReconDir/TrackerECal",
                     "ReconDir/Global",
                     #"ReconDir/Tracker",
                     #"ReconDir/SMRD",
                     #"ReconDir/FGDOnly",
                     "ReconDir/ECalTestbeam",
                     "TruthDir/Trajectories",
                     "TruthDir/Vertices",
                     "TruthDir/GRooTrackerVtx",
                     "TruthDir/NRooTrackerVtx",
                    ]

allInputTrees = ["HeaderDir/BasicHeader",
                     "HeaderDir/BasicDataQuality",
                     "HeaderDir/BeamSummaryData",
                     "HeaderDir/GeometrySummary",
                     "ReconDir/P0D",
                     "ReconDir/P0DECal",
                     "ReconDir/TrackerECal",
                     "ReconDir/Global",
                     "ReconDir/Tracker",
                     "ReconDir/SMRD",
                     "ReconDir/FGDOnly",
                     "ReconDir/ECalTestbeam",
                     "TruthDir/Trajectories",
                     "TruthDir/Vertices",
                     "TruthDir/GRooTrackerVtx",
                     "TruthDir/NRooTrackerVtx",
                    ]

###############################################################################

class OaAnalysisFolder:
    def __init__(self):
        pass
    
    def addTree(self, treeName, tree):
        setattr(self,treeName,tree)

###############################################################################

class OaAnalysisEvent:
    def __init__(self, inputTrees):
        self._hasTruth = False
        self.treeList = []
        for (folder,treeName),tree in inputTrees.iteritems():
            if not hasattr(self, folder):
                if folder=="TruthDir":
                    self._hasTruth = True
                folderObj = OaAnalysisFolder()
                setattr(self, folder, folderObj)
            folderObj = getattr(self,folder)
            folderObj.addTree(treeName,tree)
            self.treeList.append(tree) 
            
    def getEntry(self, i):
        result = True
        for tree in self.treeList:
            r = tree.GetEntry(i)
            result = result and r
        return result
    
    def hasTruth(self):
        return self._hasTruth
        

###############################################################################

class OaAnalysisEventLoop:
    def __init__(self):
        self.inputTrees = dict()
    
    def addTree(self, folder, treeName, tree):
        self.inputTrees[(folder,treeName)] = tree
        
    def hasTree(self, folder, treeName):
        return (folder,treeName) in self.inputTrees
        
    def __iter__(self):
        event = OaAnalysisEvent(self.inputTrees)
        i = 0
        while event.getEntry(i):
            yield event
            i += 1
            
    def _setInputFileHandle(self, inputFileHandle):
        '''Set reference to the input file.
        Just holds a reference to the inputfile to prevent it being deleted if it goes out of scope.
        '''
        self.inputFileHandle = inputFileHandle
        
    @staticmethod
    def createEventLoopFromFile(inputFileName, inputTrees=defaultInputTrees):
        #open input file
        inputFile = ROOT.TFile(inputFileName,"read")
        #load trees into event loop
        result = OaAnalysisEventLoop()
        result._setInputFileHandle(inputFile)
        for name in inputTrees:
            tree = inputFile.Get(name)
            if tree:
                folder,treeName = name.split("/")
                result.addTree(folder,treeName,tree)
        return result
    
    def __len__(self):
        r = 0
        for tree in self.inputTrees.values():
            if tree:
                r = max(r, int(tree.GetEntriesFast()))
        return r

###############################################################################

class EventPot:
    def __init__(self):
        self.tryMcPot = True
        
    def get(self, event):
        pot = 0.0
        #data
        beamSummaryData = event.HeaderDir.BeamSummaryData.BeamSummaryData
        if beamSummaryData.GetEntries()>0:
            pot = beamSummaryData[0].CT5ProtonsPerSpill
        #mc
        truthVetrices = None
        try:
                truthVetrices = event.TruthDir.GRooTrackerVtx
        except AttributeError:
            try:
                truthVetrices = event.TruthDir.NRooTrackerVtx
            except AttributeError:
                #failed to get truth, never try again.
                self.tryMcPot = False
        if self.tryMcPot and truthVetrices and truthVetrices.NVtx>0:
            pot = truthVetrices.Vtx[0].OrigTreePOT
            self.tryMcPot = False
        return pot

