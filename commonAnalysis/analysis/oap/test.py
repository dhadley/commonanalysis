import ROOT

from commonAnalysis.analysis import oap

from commonAnalysis.analysis import datasets
from commonAnalysis.plot import drawOptions
from commonAnalysis.plot import drawTools
import itertools

###############################################################################

def _example1():
    #Plot track momentum

    #algorithms
    nPids = oap.alg.EventVariable("ReconDir.Global.NPIDs")
    trackMomentum = oap.alg.TrackVariable("FrontMomentum")
    hist1 = oap.data.Histogram1D("nPids", "nPids", nPids, ROOT.RooBinning(100, 0.0, 100.))
    hist2 = oap.data.Histogram1D("trackP", "track momentum", trackMomentum, ROOT.RooBinning(100, 0.0, 10000.)) 
    algs = [hist1,
            hist2,
            ]
    dataset = datasets.Dataset("example1", "/home/dave/t2k/data/nd280/production005/E/mcp/genie/2010-11-*/magnet/*/anal/oa_*.root")
    job = oap.run.ProcessOaAnalysis("test.root", algs)
    job.run(dataset, 1000)
    for h in [hist1,hist2]:
        paint = drawTools.HistogramCollectionPainter() 
        c = paint.paint(h.hist)
        raw_input("wait")
    return

###############################################################################

def _example2():
    #Plot track pair invariant mass
    trackMass = oap.alg.Vectorize(oap.func.CalculateTrackInvariantMass(), 
                                         oap.alg.Combinations(oap.var.globalTracks),
                                         )
    hist = oap.data.Histogram1D("invariantMass", "track mass", trackMass, ROOT.RooBinning(100, 0.0, 10000.))
    algs = [hist]
    dataset = datasets.Dataset("example1", "/home/dave/t2k/data/nd280/production005/E/mcp/genie/2010-11-*/magnet/*/anal/oa_*.root")
    job = oap.run.ProcessOaAnalysis("test.root", algs)
    job.run(dataset, 1000)
    for h in [hist]:
        paint = drawTools.HistogramCollectionPainter() 
        c = paint.paint(h.hist)
        raw_input("wait")
    return


###############################################################################

class Cuts:
    minX = -832.2
    maxX = 832.2
    minY = -777.2
    maxY = 887.2
    minZ = 123.45
    maxZ = 446.95

class InFV():
    def __init__(self, minX=Cuts.minX, maxX=Cuts.maxX, minY=Cuts.minY, maxY=Cuts.maxY, minZ=Cuts.minZ, maxZ=Cuts.maxZ):
        self.minX = minX
        self.maxX = maxX
        self.minY = minY
        self.maxY = maxY
        self.minZ = minZ
        self.maxZ = maxZ
        
 
    def __call__(self, track):
        x = track.FrontPosition
        return (self.minX < x.X() < self.maxX 
                and self.minY < x.Y() < self.maxY 
                and self.minZ < x.Z() < self.maxZ
                )

###############################################################################

def _example3():
    #electron analysis
    #track pairs starting in the FGD,
    #invariant mass < cut
    #pull cut
    
    fgdTracks = oap.alg.Filter(InFV(),
                                      oap.var.globalTracks,
                                      )
    trackMass = oap.alg.Vectorize(oap.func.CalculateTrackInvariantMass(), 
                                         oap.alg.Combinations(fgdTracks),
                                         )
    hist = oap.data.Histogram1D("invariantMass", "track mass", trackMass, ROOT.RooBinning(100, 0.0, 10000.))
    algs = [hist,
            ]
    dataset = datasets.Dataset("example1", "/home/dave/t2k/data/nd280/production005/E/mcp/genie/2010-11-*/magnet/*/anal/oa_*.root")
    job = oap.run.ProcessOaAnalysis("test.root", algs)
    job.run(dataset, 10000)
    for h in [hist]:
        paint = drawTools.HistogramCollectionPainter() 
        c = paint.paint(h.hist)
        raw_input("wait")
    return

###############################################################################

def _main():
    _example3()
    return

###############################################################################

if __name__ == "__main__":
    _main()
