import ROOT

from commonAnalysis.analysis import oaAnalysisEvent
from commonAnalysis import tools

import var

###############################################################################

class ProcessOaAnalysis:
    
    def __init__(self, outputFile, algs, event=None):
        if event is None:
            event = var.defaultEvent
        self.event = event
        self._outputFile = outputFile
        self._algs = algs
    
    def run(self, inputDataset, nEvents=None):
        #load the libReadOaAnalysis
        tools.libraries.loadLibReadOaAnalysis(inputDataset, autoVersion=True)
        #open output file
        outputFile = self._outputFile
        if isinstance(outputFile, basestring):
            outputFile = ROOT.TFile(outputFile,"RECREATE")
        #process input data
        eventCount = 0
        breakFlag = False
        #loop over input files
        nFiles = len(inputDataset)
        if nFiles>1:
            iterFiles = tools.progress.printProgress("ProcessOaAnalysis",nFiles, inputDataset)
        else:
            iterFiles = inputDataset
        for fname in iterFiles:
            eventLoop = oaAnalysisEvent.OaAnalysisEventLoop.createEventLoopFromFile(fname)
            if nFiles==1:
                eventLoop = tools.progress.printProgress("ProcessOaAnalysis",len(eventLoop),eventLoop)
            for event in eventLoop:
                self.event.currentEvent = event
                for alg in self._algs:
                    alg()
                eventCount += 1
                breakFlag = not (nEvents is None) and eventCount > nEvents
                if breakFlag:
                    break
            if breakFlag:
                break    
        #write output
        for alg in self._algs:
            alg.write(outputFile)
        return

###############################################################################

class Selection:
    
    def __init__(self, alg, event=None):
        if event is None:
            event = var.defaultEvent
        self.event = event
        self.alg = alg
    
    def applySelection(self, event):
        self.event.currentEvent = event
        result = self.alg()
        return bool(result)
    
    def getCutFlow(self):
        return None
        
###############################################################################