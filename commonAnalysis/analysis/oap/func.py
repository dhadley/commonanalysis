import ROOT

###############################################################################

class CalculateTrackInvariantMass:
    def __call__(self, tracks):
        v = ROOT.TLorentzVector(0.0, 0.0, 0.0, 0.0)
        for t in tracks:
            v += self._getVec(t)
        invMass = v.M()
        return invMass
    
    def _getVec(self, track):
        p = track.FrontMomentum
        d = track.FrontDirection
        v3 = ROOT.TVector3(p*d.X(),p*d.Y(),p*d.Z())
        v4 = ROOT.TLorentzVector(v3,v3.Mag())
        return v4
    
###############################################################################
