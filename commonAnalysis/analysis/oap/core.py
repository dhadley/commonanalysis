
###############################################################################

class Algorithm:
    
    def __call__(self):
        return list(self)
    
    def write(self, outFile):
        pass
    
    def __iter__(self):
        yield self()

###############################################################################

class Event(Algorithm):
    
    def __init__(self):
        self.currentEvent = None    
    
    def __call__(self):
        return self.currentEvent

###############################################################################

defaultEvent = Event()

###############################################################################