import itertools

import ROOT

from commonAnalysis.plot import drawTools

import core
import alg

###############################################################################

class Histogram1D(core.Algorithm):
    def __init__(self, name, title, xVal, xBinning, category=None, weight=None):
        if category is None:
            category = itertools.cycle([title])
        if weight is None:
            weight = itertools.cycle([1.0])
        self.xVal = xVal
        self.xBinning = xBinning
        self.category = category
        self.weight = weight
        self.hist = drawTools.HistogramCollection1D(name, title, xBinning)
    
    def __call__(self):
        xValues = iter(self.xVal)
        categories = iter(self.category)
        weights = iter(self.weight)
        for x, cat, weight in itertools.izip(xValues, categories, weights):
            self.hist.fill(cat, weight, x)            
        return

###############################################################################