import ROOT

import core 
import var

from commonAnalysis.plot import drawTools
from commonAnalysis.tools import progress
from commonAnalysis import tools
import itertools
from commonAnalysis.analysis import matchingUtilities

###############################################################################

class EventVariable(core.Algorithm):
    def __init__(self, name, event=None):
        if event is None:
            event = core.defaultEvent
        self.event = event
        self._names = name.split(".")
        
    def __call__(self):
        attr = self.event()
        for n in self._names:
            attr = getattr(attr, n)
        #print "attr=",attr
        return attr

###############################################################################

class EventContainer(core.Algorithm):
    def __init__(self, name, event=None):
        if event is None:
            event = core.defaultEvent
        self.event = event
        self._names = name.split(".")
        
    def __iter__(self):
        attr = self.event()
        for n in self._names:
            attr = getattr(attr, n)
        #print "attr=",attr
        for x in attr:
            yield x

###############################################################################

class TrackVariable(core.Algorithm):
    def __init__(self, name, tracks=None):
        if tracks is None:
            tracks = var.globalTracks
        self.tracks = tracks
        self._names = name.split(".")
        
    def __call__(self):
        return list(self)
    
    def __iter__(self):
        tracks = self.tracks
        for t in tracks:
            attr = t
            for n in self._names:
                attr = getattr(attr, n)
                yield attr

###############################################################################

class Constant(core.Algorithm):
    def __init__(self, value):
        self.value = value
        
    def __call__(self):
        return self.value

###############################################################################

class Vectorize:
    def __init__(self, function, iterable):
        self.function = function
        self.iterable = iterable
        
    def __iter__(self):
        for val in self.iterable:
            yield self.function(val)

###############################################################################

class Call:
    def __init__(self, function, *args):
        self.function = function
        self.args = args
        
    def __call__(self):
        result = self.function(*self.args)
        return result

###############################################################################

class Iterate:
    def __init__(self, callable):
        self.callable = callable
        
    def __iter__(self):
        r = self.callable()
        for v in r:
            yield v


###############################################################################

class Filter(core.Algorithm):
    def __init__(self, function, iterable):
        self.iterable = iterable
        self.function = function
        
    def __iter__(self):
        for v in self.iterable:
            if self.function(v):
                yield v

###############################################################################

def Combinations(iterable, n=2):
    return Iterate(Call(itertools.combinations, iterable, n))

###############################################################################

class Volume:
    def __init__(self, xMin, xMax, yMin, yMax, zMin, zMax):
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax
        self.zMin = zMin
        self.zMax = zMax
        
    def __contains__(self, position):
        x = position
        return (self.xMin < x.X() < self.xMax 
                and self.yMin < x.Y() < self.yMax 
                and self.zMin < x.Z() < self.zMax
                )

###############################################################################

class InFiducialVolume():
    def __init__(self, *volumes):
        self.volumes = volumes
 
    def __call__(self, track):
        x = track.FrontPosition
        return any((x in v for v in self.volumes))

###############################################################################

class All:
    def __init__(self, function):
        self.function = function
        
    def __call__(self, iterable):
        f = self.function
        return all((f(x) for x in iterable))

###############################################################################

class TrackTrueParticle(core.Algorithm):
    def __init__(self, tracks=None, event=None):
        if event is None:
            event = var.defaultEvent
        self.event = event
        if tracks is None:
            self.tracks = var.globalTracks
        self.tracks = tracks
    
    def __iter__(self):
        event = self.event()
        for t in self.tracks:
            et = None
            if event.hasTruth():
                truth = matchingUtilities.matchGlobalToTruthTrajectory(event, t)
                et = str(tools.pdgDatabase.GetParticle(truth.PDG).GetName())
            else:
                et = "data"
            yield et

###############################################################################
