import ROOT
import datetime
import os
import numpy
import shutil
import tempfile
from ..tools import progress
from ..tools import hosts
from ..analysis import oaAnalysisEvent

###############################################################################

class SkimOaAnalysisException(Exception):
    pass       

###############################################################################

class SkimOaAnalysis:
    """Select specific events from a set of oaAnalysis files."""
    def __init__(self, inputDataset, outputFileName, selection, requiredTreeNames=None, optionalTreeNames=None, copyToTemp=None, overrideMCPotPerFile=None):
        """Construct an new skimming object. 
        Requires an list of input file names, 
        the name of the output file (existing files will be overwritten).
        Selection must be an object which provides a method "applySelection(event)".
        This method must return True (event passed selection) or False (event did not pass selection).
        The input event is a regular TTree which has all oaAnalysis trees added as friends.
        NB there may be problems caused if oaAnalysis trees contain objects with the same name.
        Run the job by calling "run()".
        """
        self.inputDataset = inputDataset
        self.outputFileName = outputFileName
        
        self.selection = selection
        if copyToTemp is None:
            copyToTemp = hosts.isHost(hosts.warwickCluster) or hosts.isHost(hosts.cscDesktop) #or hosts.isHost(hosts.daveLaptop)
        self.copyToTemp = copyToTemp
        self.writeOutputFileToTemp = copyToTemp
        self.overrideMCPotPerFile = overrideMCPotPerFile
        #Trees to be written out
        if requiredTreeNames is not None:
            self.requiredTreeNames = requiredTreeNames
        else:
            self.requiredTreeNames = ["HeaderDir/BasicHeader",
                              "HeaderDir/BasicDataQuality",
                              "HeaderDir/BeamSummaryData",
                              "HeaderDir/GeometrySummary",
                              "ReconDir/P0D",
                              "ReconDir/P0DECal",
                              "ReconDir/TrackerECal",
                              "ReconDir/Global",
                              "ReconDir/Tracker",
                              "ReconDir/SMRD",
                              "ReconDir/FGDOnly",
                              ]
        if optionalTreeNames is not None:
            self.optionalTreeNames = optionalTreeNames
        else:
            self.optionalTreeNames = ["TruthDir/Trajectories",
                                  "TruthDir/Vertices",
                                  "TruthDir/GRooTrackerVtx",
                                  "TruthDir/NRooTrackerVtx",
                                  ]
        self.listOfTreeNames = self.requiredTreeNames+self.optionalTreeNames
        self.percentageComplete = 0
        self.sanityChecks()
        
    def sanityChecks(self, doCheckTrees=False):
        
        #check that the input files exists
        for f in self.inputDataset.iterFiles():
            if not os.path.exists(f):
                raise SkimOaAnalysisException("missing input file",f)
        #check that selection meets our requirements
        if not hasattr(self.selection, "applySelection"):
            raise SkimOaAnalysisException("selection has no applySelection attribute",
                                          selection,
                                          outputFileName,
                                          )
        #check that applySelection is a method of the class
        elif not callable(self.selection.applySelection):
            raise SkimOaAnalysisException("selection has an applySelection but it is not callable.",
                                          selection, selection.applySelection,
                                          outputFileName
                                          )
        #check that each tree has only been set once
        if not len(set(self.listOfTreeNames))==len(self.listOfTreeNames):
            raise SkimOaAnalysisException("duplicate tree name in list of trees.",
                                          listOfTreeNames,
                                          outputFileName
                                          )
        
        if doCheckTrees:
            #open all of the input files and check that they all have the same set of trees in them.
            treesInFirstFile = []
            treeSet = set(self.listOfTreeNames)
            requiredSet = set(self.requiredTreeNames)
            optionalSet = set(self.optionalTreeNames)
            nFiles = len(self.inputDataset)
            iterFiles = progress.printProgress("SkimOaAnalysis.sanityChecks",nFiles,self.inputDataset)
            for f in iterFiles:
                tfile = ROOT.TFile(f,"read")
                #get a list of trees in the file 
                treesInFile = set([ t for t in treeSet if tfile.Get(t)])
                if len(treesInFirstFile)==0:
                    treesInFirstFile = treesInFile
                #check that all required trees are in the file
                missingRequiredTrees = requiredSet - treesInFile
                if len(missingRequiredTrees):
                    raise Exception("required TTrees are missing from an input file",missingRequiredTrees,f)
                #check that any optional trees are present, 
                #provided they were also present in previous files.
                missingOptionalTrees = optionalSet - treesInFile #set of missing optional trees
                missingOptionalTreesInFirst = missingOptionalTrees & treesInFirstFile #set of missing tress also in first file
                if len(missingOptionalTreesInFirst):
                    raise Exception("optional trees present in the first given file, but not present in a later file",missingOptionalTreesInFirst,f)
        return
    
    def getTreeNames(self):
        return self.listOfTreeNames
        
    def run(self):
        """Run the skimming job."""
        outFile = None
        outTrees = None
        potTree = None
        tmpDir = None
        tmpOutputFileName = self.outputFileName
        #check that output directory exists
        outputDir = os.path.dirname(tmpOutputFileName) 
        if not os.path.exists(outputDir):
            #does not exist, create it
            os.makedirs(outputDir)
        if self.copyToTemp:
            #Create directory in temp
            tmpDir = tempfile.mkdtemp()
            if self.writeOutputFileToTemp:
                tmpOutputFileName = tmpDir + "/" + os.path.basename(self.outputFileName)
        print self.outputFileName
        print tmpOutputFileName
        #loop over input files
        nFiles = len(self.inputDataset)
        if nFiles>1:
            iterFiles = progress.printProgress("SkimOaAnalysis.run",nFiles,self.inputDataset)
        else:
            iterFiles = self.inputDataset
        for fname in iterFiles:
            if self.copyToTemp:
                #copy file to temp directory
                newFname = tmpDir + "/" + os.path.basename(fname)
                shutil.copy(fname,newFname)
                fname = newFname
            #open input file
            inFile = ROOT.TFile(fname,"read")
            inTrees = dict()
            for treeName in self.listOfTreeNames:
                tree = inFile.Get(treeName)
                if tree:
                    key = tuple(treeName.split("/"))
                    #print key
                    inTrees[key] = tree
            if outFile:
                #reset branch addresses
                for key,trOut in outTrees.iteritems():
                    trIn = inTrees[key]
                    trIn.CopyAddresses(trOut)
            else:
                #create output file
                outFile = ROOT.TFile(tmpOutputFileName,"recreate")
                self._writeOneOffObjects(inFile,outFile)
                #create output trees
                outTrees = dict()
                for key,tree in inTrees.iteritems():
                    dirName = key[0]
                    if not outFile.GetDirectory(dirName):
                        outFile.mkdir(dirName)
                    outFile.cd(dirName)
                    outTrees[key] = tree.CloneTree(0)
                    outTrees[key].SetAutoSave(1000000000/len(inTrees))
                    outFile.cd()
                #create pot tree
                outFile.cd()
                self.potTree = ROOT.TTree("PotTree","tree containing per file level information such as POT.")
                self.createPotTreeBranches()
            #add friends to a tree to loop over
            eventLoop = oaAnalysisEvent.OaAnalysisEventLoop()
            for (folder,treeName),tree in inTrees.iteritems():
                eventLoop.addTree(folder,treeName,tree)
            #load trees
            if nFiles==1:
                eventLoop = progress.printProgress("SkimOaAnalysis.run",len(eventLoop),eventLoop)
            for event in eventLoop:
                #print event,tree.GetName()
                if self.selection.applySelection(event):
                    #fill trees
                    for key,tree in outTrees.iteritems():
                        dirName = key[0]
                        outFile.cd(dirName)
                        tree.Fill()
                        #force write to disc to minimise memory usage.
                        #NB : doing this each event may be sub-optimal
                        #Optimising AutoSave and AutoFlush options would be better,
                        #but this behaviour seems to be ROOT version dependent.
                        #for now we accept poor IO performance to guarantee
                        #minimal memory usage.
                        tree.FlushBaskets()
                pot = self.setPotTreeValues(event, tfile=inFile)
                eventWeight = 1.0
                cutFlow = self.selection.getCutFlow()
                if cutFlow:
                    cutFlow.fill(pot,eventWeight)
            #save the tree contents
            #for key, tree in outTrees.iteritems():
            #    tree.AutoSave()    
            #write a POT entry
            outFile.cd()
            self.potTree.Fill()
            self.resetPotValues()
            inFile.Close()
            #delete the temporary file
            if self.copyToTemp:
                os.remove(newFname)
        #write output trees
        if outFile:
            cutFlow = self.selection.getCutFlow()
            if cutFlow:
                cutFlow.writeToFile(outFile)
            outFile.Write()
            outFile.Close()
        #finally copy the output file to destination and delete all temporary files
        if self.copyToTemp:
            if not tmpOutputFileName == self.outputFileName:
                shutil.copy(tmpOutputFileName,self.outputFileName)
            shutil.rmtree(tmpDir,ignore_errors=True)
        #print messages about the file size reduction rate
        try:
            inputFileSize = self.inputDataset.getTotalFileSize()
            outputFileSize = os.path.getsize(self.outputFileName)
            frac = 0.0
            if inputFileSize>0:
                frac = 100.0*(float(outputFileSize)/float(inputFileSize))
            reduction = 0.0
            if outputFileSize>0:
                reduction = float(inputFileSize)/float(outputFileSize)
            convertToMB = 9.53674e-7
            print "SkimOaAnalysis.run","inputFileSize =","%.1f"%(inputFileSize*convertToMB),"MB"
            print "SkimOaAnalysis.run","outputFileSize =","%.1f"%(outputFileSize*convertToMB),"MB"
            print "SkimOaAnalysis.run","selected fraction =","%.0f"%frac,"%"
            print "SkimOaAnalysis.run","reduction factor =","%.0f"%reduction
        except os.error:
            print "ERROR: unable to get output file size."
        return
    
    def createPotTreeBranches(self):
        self.PotValue = numpy.array([0.0])
        self.PotBranch = self.potTree.Branch("Pot", self.PotValue, "Pot/D")
        self.CT5ProtonsPerSpillValue = numpy.array([0.0])
        self.CT5ProtonsPerSpillBranch = self.potTree.Branch("CT5ProtonsPerSpill", self.CT5ProtonsPerSpillValue, "CT5ProtonsPerSpill/D")
        self.PotWithND280OffFlagValue = numpy.array([0.0])
        self.PotWithND280OffFlagBranch = self.potTree.Branch("PotWithND280OffFlag", self.PotWithND280OffFlagValue, "PotWithND280OffFlag/D")
        self.PotWithMAGNETFlagValue = numpy.array([0.0])
        self.PotWithMAGNETFlagBranch = self.potTree.Branch("PotWithMAGNETFlag", self.PotWithMAGNETFlagValue, "PotWithMAGNETFlag/D")    
        self.OrigTreePOTValue = numpy.array([0.0])
        self.OrigTreePOTBranch = self.potTree.Branch("OrigTreePOT", self.OrigTreePOTValue, "OrigTreePOT/D")
        self.PotRunIDValue = numpy.array([0.0])
        self.PotRunIDBranch = self.potTree.Branch("RunID", self.PotRunIDValue, "RunID/D")
        self.PotFileNameValue = ROOT.TObjString("")
        self.PotFileNameBranch = self.potTree.Branch("FileName", self.PotFileNameValue)
        self.tryMcPot = True   

    def resetPotValues(self):
        self.PotValue[0] = 0.0
        self.CT5ProtonsPerSpillValue[0] = 0.0
        self.PotWithND280OffFlagValue[0] = 0.0
        self.PotWithMAGNETFlagValue[0] = 0.0
        self.OrigTreePOTValue[0] = 0.0
        self.tryMcPot = True # force one attempt per file
        return
    
    def setPotTreeValues(self, event, tfile):
        pot = 0.0
        runID = event.HeaderDir.BasicHeader.RunID
        self.PotRunIDValue[0] = float(runID)
        #data
        beamSummaryData = event.HeaderDir.BeamSummaryData.BeamSummaryData
        self.PotFileNameValue.SetString(tfile.GetName())
        if beamSummaryData.GetEntries() > 0:
            pot = beamSummaryData[0].CT5ProtonsPerSpill
            self.CT5ProtonsPerSpillValue[0] += pot
            #print event.ND280OffFlag,pot
            dataQuality = event.HeaderDir.BasicDataQuality
            if dataQuality.ND280OffFlag == 0:
                self.PotValue[0] += pot #default pot value assumes a cut on offline flag
                self.PotWithND280OffFlagValue[0] += pot
            if dataQuality.MAGNETFlag == 0:
                self.PotWithMAGNETFlagValue[0] += pot
        #mc
        if self.tryMcPot:
            truthVetrices = None
            try:
                truthVetrices = event.TruthDir.GRooTrackerVtx
            except AttributeError:
                try:
                    truthVetrices = event.TruthDir.NRooTrackerVtx
                except AttributeError:
                    #failed to get truth, never try again.
                    self.tryMcPot = False
            if truthVetrices:
                #found truth vertices, get pot
                if truthVetrices.NVtx>0:
                    pot = truthVetrices.Vtx[0].OrigTreePOT
                    if self.overrideMCPotPerFile is not None:
                        #Replace POT with user defined value
                        pot = self.overrideMCPotPerFile 
                    self.OrigTreePOTValue[0] += pot
                    self.PotValue[0] += pot
                    #only get mc pot once (per file)
                    self.tryMcPot = False
        return pot
    
    def _writeOneOffObjects(self, inFile, outFile):
        #write ToaAnalysisUtils
        utils = inFile.Get("UtilsDir/Utils")
        if not utils:
            utils = inFile.Get("UtilsDir/ND::ToaAnalysisUtils")
        outFile.mkdir("UtilsDir")
        outFile.cd("UtilsDir")
        utils.Write()
        outFile.cd()
        #write modules
        for k in inFile.GetListOfKeys():
            if k.GetClassName()=="TDirectoryFile":
                directory = inFile.Get(k.GetName())
                modulesDir = directory.Get("Modules")
                if modulesDir:
                    outFile.mkdir(directory.GetName())
                    outDir = outFile.GetDirectory(directory.GetName())
                    outDir = outDir.mkdir("Modules")
                    for l in modulesDir.GetListOfKeys():
                        obj = modulesDir.Get(l.GetName())
                        outDir.cd()
                        obj.Write()
        return
            
    
###############################################################################

def simpleTest():
    import glob
    import os
    #files to process
    #inputFilePattern = "/home/dave/t2k/data/nd280/production005/A/rdp/ND280/00006000_00006999/anal/oa_nd_spl_*.root"
    inputFilePattern = "/home/dave/t2k/data/nd280/production005/A/mcp/genie/2010-11-water/magnet/beamb/anal/oa_*_beam_*.root"
    inputFileList = glob.glob(inputFilePattern)
    #just do two files as a test
    inputFileList = inputFileList[:3]
    print inputFileList
    #load the oaAnalysisLibrary
    if not os.path.exists("libReadoaAnalysis"):
        tfile = ROOT.TFile(inputFileList[0],"read")
        tfile.MakeProject("libReadoaAnalysis","*","new+")
    ROOT.gSystem.Load("libReadoaAnalysis/libReadoaAnalysis.so")
    
    #a simple example selection class
    class SimpleSandMuonSelection:
        """Selects events that contain a track with 3 TPC components.
        In beam these are very likely to contain sand muons.
        """
        def applySelection(self, event):
            #any global tracks have 3 TPCs?
            for pid in event.PIDs:
                if pid.NTPCs>=3:
                    return True
            return False
    #run skim job
    outputFileName = "simpleTest.skimmer.oaAnalysis.root"
    selection = SimpleSandMuonSelection()
    skimmer = SkimOaAnalysis(inputFileList, outputFileName, selection, copyToTemp=True)
    skimmer.run()
    #now check that the output is sensible
    tfile = ROOT.TFile(outputFileName,"read")
    treeNames = skimmer.listOfTreeNames
    trees = dict([(n,tfile.Get(n)) for n in treeNames if n not in skimmer.optionalTreeNames])
    lastIds = None
    for tree in trees.itervalues():
        ids = [ (int(e.RunID),int(e.EventID)) for e in tree ]
        #check each entry should be unique
        assert len(set(ids)) == len(ids)
        if lastIds:
            #check that event ids are identical in each tree
            assert ids == lastIds
        lastIds = ids
    #done!
    return

###############################################################################                

def main():
    simpleTest()

if __name__ == "__main__":
    main()

