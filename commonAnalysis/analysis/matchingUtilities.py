
def matchGlobalToTrackerECal(event,globalEcalObj):
    matchedObj = None
    if globalEcalObj:
        #find the corresponding ECAL object
        ecalObjects = event.ReconDir.TrackerECal.ReconObject
        for ecalObj in ecalObjects:
            if globalEcalObj.UniqueID == ecalObj.UniqueID:
                matchedObj = ecalObj
                break
    return matchedObj

def matchTrackerECalToGlobal(event,ecalObj):
    matchedObj = None
    if ecalObj:
        #find the corresponding ECAL object
        globalTracks = event.ReconDir.Global.PIDs
        for pid in globalTracks:
            for globalEcalObj in pid.ECAL:
                if globalEcalObj.UniqueID == ecalObj.UniqueID:
                    matchedObj = pid
                    break
    return matchedObj

def matchGlobalToTruthTrajectory(event,globalTrack):
    #find a truth match
    truthMatch = None
    if event.hasTruth() and globalTrack:
        id = globalTrack.TrueParticle.ID
        for truthTraj in event.TruthDir.Trajectories.Trajectories:
            if truthTraj.ID == id:
                truthMatch = truthTraj
                break
    return truthMatch

def matchTrackerECalToTruthTrajectory(event,ecalObj):
    #find a truth match
    truthMatch = None
    if event.hasTruth() and ecalObj:
        id = ecalObj.G4ID
        for truthTraj in event.TruthDir.Trajectories.Trajectories:
            if truthTraj.ID == id:
                truthMatch = truthTraj
                break
    return truthMatch

def matchTruthTrajectoryToTruthVertex(event, trajectory):
    match = None
    tid = trajectory.ID
    vertices = event.TruthDir.Vertices.Vertices
    for vertex in vertices:
        for id in vertex.PrimaryTrajIDs:
            if tid == id:
                match = vertex
                break
    if match is None:
        #try the parent trajectory
        for parent in event.TruthDir.Trajectories.Trajectories:
            if parent.ID == trajectory.ParentID:
                match = matchTruthTrajectoryToTruthVertex(event, parent)
    return match
