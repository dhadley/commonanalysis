"""Provide several useful tools for analysing oaAnalysis files in pyroot.

The commonAnalysis package provides the following features:

* Handles loading of libraries for analysing oaAnalysis files.
* Skimming of oaAnalysis files.
* Easy generation of flat ntuples from oaAnalysis files. 
* Easy generation of plots from these ntuples.
* Easy job submission to the batch system.

The commonAnalysis package will eventually provide the following features:

* Well-Documented (I hope!).

For the tutorials see the subpackage :py:mod:`~commonAnalysis.examples`.
    
Note: The aim is for this package to be as simple as possible. 
Hence, it's contents should be limited to useful tools for running oaAnalysis 
jobs on the Warwick computer systems. 
If you are trying to add something beyond-this-scope you should create another 
package that depends on this one, or fork the project.

drh 2012-08-05
"""

import ROOT#@UnresolvedImport
ROOT.PyConfig.IgnoreCommandLineOptions = True # Prevent ROOT from hi-jacking --help

#Import sub-packages
import analysis
import batch
import examples
import plot
import tools