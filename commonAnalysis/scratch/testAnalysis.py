"""An example analysis.

This example analysis will show how to:
    * Make an FGD skim.
    * Make an ntuple from the skim file.
    * Make some basic physics plots
    * Calculate POT, normalise plots by it.
    * Save the cut flow at each stage.
    * Plot the cut flow.
    * Easily split jobs up for running on the batch system.
"""

from ..analysis import cutTools
from ..analysis import skimmer
from ..analysis import datasets
from ..analysis import oaAnalysisEvent
from ..tools import libraries
from ..tools import commandLine
from ..batch import warwickCluster
from ..tools import ntuple
from .. import plot

import ROOT
import operator


###############################################################################

class InFgdFiducialVolumeSelection(cutTools.BaseSelection):
    
    def __init__(self, name="InFgdFiducialVolumeSelection", title="global track starts in FGD FV"):
        cutTools.BaseSelection.__init__(self,name,title)
        #setup the cutflow
        cutFlow = self.getCutFlow()
        self.cutHasGlobalPid = cutFlow.registerCut("hasGlobalPid","at least one global PID object in event")
        self.cutInFV = cutFlow.registerCut("inFV","one global pid with Front position inside the FGD fiducial volume")
    
    def applySelection(self, event):
        result = False
        #is there an interaction reconstructed in an FGD?
        globalTracks = event.ReconDir.Global.PIDs
        if globalTracks.GetEntries():
            self.cutHasGlobalPid.setPassed()
            for track in globalTracks:
                if self.inFiducialVolume(track.FrontPosition):
                    self.cutHasGlobalPid.setPassed()
                    result = True
                    break
        return result
    
    def inFiducialVolume(self, vertexPosition):
        x = vertexPosition.X()
        y = vertexPosition.Y()
        z = vertexPosition.Z()
        fgd1Cuts = ( -832.2 < x < 832.2, -777.2 < y < 887.2, 123.45 < z < 446.95 )
        fgd2Cuts = ( -832.2 < x < 832.17, -777.2 < y < 887.17, 1481.45 < z < 1807.95 )
        result = all(fgd1Cuts) or all(fgd2Cuts)
        return result
    
###############################################################################

def testMakeSkim(options):
    #select the input dataset
    inputDataSet = datasets.DatasetDefs.mcp_production5A_genie_magnet_2010_11_water
    #load the libReadOaAnalysis
    libraries.loadLibReadOaAnalysis(inputDataSet)
    #select a subset of the data
    chunk = options.get("chunk")
    nChunks = options.get("nChunks")
    print inputDataSet.getFileList()
    inputDataSet = inputDataSet.getChunk(chunk,nChunks)
    print inputDataSet.getFileList()
    #setup event selection
    eventSelection = InFgdFiducialVolumeSelection()
    #run the skim
    outputFileName = "skim.test.inFgd.%s.root"%inputDataSet.getName()
    skim = skimmer.SkimOaAnalysis(inputDataset=inputDataSet, 
                                  outputFileName=outputFileName, 
                                  selection=eventSelection, 
                                 )
    skim.run()
    
    return

###############################################################################

class NtupleMaker:
    def __init__(self, name, title, inputDataset, outputFileName, selection):
        self.name = name
        self.title = title
        self.inputDataset = inputDataset
        self.outputFileName = outputFileName
        self.cutFlow = cutTools.CutFlow(name,"Cut flow for "+title)
        
    def setupOutput(self):
        self.outFile = ROOT.TFile(self.outputFileName,"recreate")
        self.outTree = ROOT.TTree("ntuple", "ntuple from"+self.inputDataset.getName())
        self.trackMomentum = ntuple.BranchPrimative("trackMomentum", self.outTree)
        self.muonPull = ntuple.BranchPrimative("muonPull", self.outTree)
        return
        
    def processEvent(self, event):
        #cut 0 : track in beam timing window
        #TODO
        #cut 1 : track has TPC component
        selectedTrack = max( event.ReconDir.Global.PIDs, key=operator.attrgetter("FrontMomentum") )
        if selectedTrack.NTPCs>0:
            #fill branches
            self.trackMomentum.setValue( selectedTrack.FrontMomentum )
            self.muonPull.setValue( selectedTrack.TPC[0].PullMuon )
        self.outTree.Fill()
        return
    
    def run(self):
        self.setupOutput()
        loadCf = cutTools.CutFlowLoader(self.name,self.title)
        for fileName in self.inputDataset:
            loadCf.load(fileName)
            eventLoop = oaAnalysisEvent.OaAnalysisEventLoop.createEventLoopFromFile(fileName)
            for event in eventLoop:
                self.processEvent(event)
        previousCutFlow = loadCf.getCutFlow()
        mergedCutFlow = cutTools.CutFlow.merge(first=previousCutFlow, second=self.cutFlow)
        self.outFile.cd()
        self.outTree.Write()
        mergedCutFlow.writeToFile(self.outFile)
        self.outFile.Close()
        

###############################################################################

def testMakeNtuple():
    #select the input dataset
    inputDataset = datasets.Dataset("fgdSkims","skim.test.inFgd.*.root")
    #load the libReadOaAnalysis
    libraries.loadLibReadOaAnalysis(inputDataset)
    #run the ntuple maker
    outputFileName = "ntuple."+inputDataset.getName()+".root"
    ntupleMaker = NtupleMaker("fgdNtuple",
                              "an example fgd tuple",
                              inputDataset, 
                              outputFileName)
    ntupleMaker.run()
    return

###############################################################################

def testMakePlots():
    inputDataset = datasets.Dataset("fgdNtuples","ntuple.fgdSkims.root")
    output = plot.io.CanvasWriter("testPlots")
    plotter = plot.drawTools.TreeDrawer(inputDataset, "ntuple")
    canvas = plotter.draw("trackMomentum")
    output.save(canvas)
    return

###############################################################################

def testSubmitJobs(options):
    nChunks = 2
    #setup jobs
    allJobs = []
    for jobNum in xrange(nChunks):
        options = options.clone()
        options.set("submitJobs", False)
        options.set("nChunks", 2)
        options.set("chunks", jobNum)
        command = "python -m commonAnalysis.scratch.testAnalysis "+options.getOptionsString()
        jobName = "testAnalysis"+str(jobNum)
        job = warwickCluster.ClusterJob(jobName, 
                                  queue="medium", 
                                  cmd=command, 
                                  workingDirectory="./", 
                                  setupND280=True
                                  )
        allJobs.append(job)
    #submit the jobs
    for job in allJobs:
        job.submit()

###############################################################################

def parseCml():
    options = commandLine.OptionsParser()
    options.addSwitchOption("submitJobs", description="submit all jobs to the batch cluster")
    options.addValueOption("chunk", description="select chuck of data to process", dataType=int, default=None)
    options.addValueOption("nChunks", description="number of chunks to break dataset up into", dataType=int, default=10)
    options.addSwitchOption("makeNtuples", description="make ntuples from the skim files")
    options.addSwitchOption("makePlots", description="make plots from the ntuples")
    options.parse()
    return options

###############################################################################

def main():
    options = parseCml()
    if options.get("submitJobs"):
        testSubmitJobs(options)
    elif options.get("chunk") is not None:
        testMakeSkim(options)
    elif options.get("makeNtuples"):
        testMakeNtuple()
    elif options.get("makePlots"):
        testMakePlots()
    return

###############################################################################

if __name__ == "__main__":
    main() 
    
