import ROOT
from ..plot import drawTools
from ..plot import drawOptions
from ..plot import io
from .. import tools
from .. import analysis

import os
import itertools

class Model:
    def __init__(self, eventType, nEvents, shape):
        self.eventType = eventType
        self.nEvents = nEvents
        self.shape = shape
        self.min = 0.0
        self.max = 10.0
        self.massFunc = ROOT.TF1("",shape,self.min,self.max)
    def generate(self):
        for i in xrange(self.nEvents):
            mass = self.massFunc.GetRandom(self.min,self.max)
            yield self.eventType,mass
        return

def makeDummyNtuple():
    outputFileName = "dummy.ntuple.root"
    if not os.path.exists(outputFileName):
        signal = "TMath::Gaus(x,5.0,.5)"
        bg1 = "x+2.0"
        bg2 = "TMath::Gaus(x,0.0,2.0)"
        nSignal = 1000
        nBg1 = 10000
        nBg2 = 5000
        nData = nSignal+nBg1+nBg2
        models = [#mc model
                  Model(0, nSignal, signal),
                  Model(1, nBg1, bg1),
                  Model(2, nBg2, bg2),
                  #data
                  Model(3, nSignal, signal),
                  Model(3, nBg1, bg1),
                  Model(3, nBg2, bg2),
                  ]
        outFile = ROOT.TFile(outputFileName, "recreate")
        outTree = ROOT.TTree("ntuple","dummy ntuple")
        br_mass = tools.ntuple.BranchPrimative("mass",outTree)
        br_eventType = tools.ntuple.BranchPrimative("eventType",outTree)
        for m in models:
            dataset = m.generate()
            for eventType,mass in dataset:
                br_mass.setValue(mass)
                br_eventType.setValue(eventType)
                outTree.Fill()
        #outTree.Draw("mass")
        #raw_input("wait")
        outTree.Write()
        outFile.Close()

def testPlots():
    ROOT.gROOT.SetBatch(True)
    inputDataset = analysis.datasets.Dataset("dummyData","dummy.ntuple.root")
    treeName = "ntuple"
    td = drawTools.TreePainter(inputDataset,treeName)
    splitDataset = drawOptions.SplitDataset()
    Cut = drawOptions.Cut
    splitDataset.add("axions",Cut("eventType==0"))
    splitDataset.add("#gamma bkg.",Cut("eventType==1"))
    splitDataset.add("e^{-} bkg.",Cut("eventType==2"))
    splitDataset.add("data",Cut("eventType==3"))
    #canv = td.paint("examplePlot1","example plot","mass", splitDataset)
    
    opt = [splitDataset, 
           drawOptions.TreatAsData("data"),
           drawOptions.Stack(drawOptions.Stack.stackMC),
           ]
    cw = io.CanvasWriter()
    canv1 = td.paintLater("examplePlot1","example plot 1","mass", 
                    drawOptions.UniformBinning(20,0.0,10.0), 
                    drawOptions.AxisLabels(x="mass [Mev]", y="num. events"), 
                    *opt)
    canv2 = td.paintLater("examplePlot2","example plot 2","mass**2", 
                    drawOptions.UniformBinning(20,0.0,100.0),
                    drawOptions.AxisLabels(x="mass^{2} [Mev^{2}]", y="num. events"), 
                    *opt)
    canv3 = td.paintLater("examplePlot3","example plot 3","TMath::Sqrt(mass)", 
                    drawOptions.AxisLabels(x="#sqrt{mass} [Mev^{1/2}]", y="num. events"),
                    drawOptions.UniformBinning(40,0.0,3.3), 
                    *opt)
    cw.save(canv1,canv2,canv3)
    #raw_input("wait")

def main():
    makeDummyNtuple()
    testPlots()

if __name__ == "__main__":
    main() 