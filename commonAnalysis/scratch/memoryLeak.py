
#A function that deliberately uses up lots of memory each iteration
def memoryLeak():
    import resource
    memoryLeak = []
    for i in xrange(1000):
        memUsageInGB = float(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)/float(1000*1000) 
        print "iteration",i,": using ",memUsageInGB,"GB"
        if memUsageInGB>4.0:
            print "reached scripted limit, breaking out of loop"
            break
        #raw_input("waiting for user input")
        memoryLeak.append(range(10000000))
    return

memoryLeak()