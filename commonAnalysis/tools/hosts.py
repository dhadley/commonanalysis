"""Determine host machine that the job is running on.
"""

import socket

###############################################################################

#list of known hosts
daveLaptop = "host_daveLaptop"
warwickCluster = "host_warwickCluster"
cscDesktop = "host_cscDesktop"
andyLaptop = "host_andyLaptop"

###############################################################################

class HostsError(Exception):
    pass

###############################################################################

# public functions

def isHost(host):
    """Determine host machine that the job is running on.
    
    :param host: The test host. It should be one of the hostnames defined at the top of this module.
    :type host: str
    :returns: bool -- the current machine host name matches the input host name.
    
    """
    #check that it is valid
    if not host in _allValidHosts:
        raise Exception("KnownHosts.isHost was provided with an unknown host",host)
    #is it equal to the current host
    return host==_currentHostId

###############################################################################

#private functions

def _getHostId(hostName):
    """
    Converts the hostname (eg reported by socket.getfqdn) to the corresponding 
    host string defined at the top of this module.
    
    :param hostName: hostname of the type returned by socket.getfqdn.
    :type hostName: str
    :returns: str -- the matching host id.
    """
    result = None
    if ".warwick.ac.uk" in hostName:
        result = cscDesktop
    elif "imogen" or "veleta" in hostName:
        result = daveLaptop
    elif "Nigel" in hostName:
        result = andyLaptop
    elif "epp-" in hostName or "comp" in hostName:
        result = warwickCluster
    #couldn't determine host ID, raise and exception.
    if result is None:
        raise HostsError("running on an unknown host", hostName)
    return result

###############################################################################

#private fields
_allValidHosts = set([daveLaptop,warwickCluster,cscDesktop,andyLaptop])
_currentHostName = socket.getfqdn() #set in KnownHosts._setup
_currentHostId = _getHostId(_currentHostName) #set in KnownHosts._setup
