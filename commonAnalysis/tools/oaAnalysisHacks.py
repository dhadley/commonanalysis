import ROOT
import os

###############################################################################

_loadedLibrary = False

###############################################################################

class LibraryNotLoadedError(Exception):
    pass

###############################################################################

def loadOaAnalysisHacks(outputPath, prod4=False):
    global _loadedLibrary
    if not _loadedLibrary:
        _loadedLibrary = True
        #names of input template and final output script/library name
        scriptName = os.environ["COMMONANALYSISROOT"]+"/commonAnalysis/tools/oaAnalysisHacks.C"
        scriptOutFileName = outputPath+"/oaAnalysisHacks.C"
        scriptLibName = outputPath+"/oaAnalysisHacks_C.so"
        #check whether we need to do anything
        noLibraryExists = not os.path.exists(scriptLibName)
        scriptHasBeenModified = noLibraryExists or os.path.getmtime(scriptName)>os.path.getmtime(scriptLibName)
        if noLibraryExists or scriptHasBeenModified:
            #create the script
            scriptInFile = open(scriptName,"r")
            scriptContent = scriptInFile.read()
            scriptContent = scriptContent.replace("{LIBOAANALYSISPATH}",outputPath)
            if prod4:
                scriptContent = scriptContent.replace("{TrajEntrancePos}","EntrancePositions")
                scriptContent = scriptContent.replace("{TrajProd4FixChangingType1}","obj->EntranceMomentum[i].Vect()")
                scriptContent = scriptContent.replace("{BeginProd4CommentOut}","/* ")
                scriptContent = scriptContent.replace("{EndProd4CommentOut}","*/ ")
                
            else:
                scriptContent = scriptContent.replace("{TrajEntrancePos}","EntrancePosition")
                scriptContent = scriptContent.replace("{TrajProd4FixChangingType1}","obj->EntranceMomentum[i]")
                scriptContent = scriptContent.replace("{BeginProd4CommentOut}"," ")
                scriptContent = scriptContent.replace("{EndProd4CommentOut}"," ")
            scriptOutFile = open(scriptOutFileName,"w")
            print >>scriptOutFile,scriptContent
            scriptOutFile.close()
        #load and compile the script with the functions needed to access the array information
        ROOT.gROOT.ProcessLine(".L "+scriptOutFileName+"+")
    return

def getInstance():
    if not _loadedLibrary:
        raise LibraryNotLoadedError()
    return ROOT.warwick.OaAnalysisHacks.get()

def getRooTrackerHepP4(vtx):
    result = []
    vec = getInstance().getRooTrackerHepP4(vtx)
    for v in vec:
        x,y,z,t = v[0],v[1],v[2],v[3]
        result.append( ROOT.TLorentzVector(x,y,z,t) )
    return result

###############################################################################

def main():
    loadOaAnalysisHacks()

if __name__ == "__main__":
    main()