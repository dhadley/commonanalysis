"""Runs the compilation of libReadoaAnalysis and loads the corresponding libraries."""

import os
import ROOT

from commonAnalysis.analysis import datasets

from . import oaAnalysisHacks
from . import fileLock

hasLoadedoaAnalysisLib = False
oaAnalysisLoadedLibName = None

def loadLibReadOaAnalysis(inputDataset, name="", prod4=False, autoVersion=False):
    '''Load libReadoaAnalysis. 
    
    Tries to load oaAnalysis libraries from the directory "./libReadoaAnalysis".
    If a name is provided it is appended to the end of the directory name. 
    If a matching library does not already it will be created from the first file in the inputDataset.
    
    :param inputDataset: the input dataset (whose first file will be used to compile libraries with MakeProject).
    :type inputDataset: :py:class:`~commonAnalysis.analysis.datasets.Dataset` 
    :param name: will be appended to the library folder. This may be needed if you need to compile multiple copies (eg from diffferent productions).
    :type name: str 
    :param prod4: a special flag when running . Changes the behaviour of :py:func:`~commonAnalysis.tools.oaAnalysisHacks.loadOaAnalysisHacks`. 
    :type prod4: bool
    '''
    global hasLoadedoaAnalysisLib
    global oaAnalysisLoadedLibName
    #do sanity checks
    namesMatch = (not hasLoadedoaAnalysisLib) or oaAnalysisLoadedLibName == name
    if not namesMatch:
        raise Exception("already loaded an oaAnalaysis library! Loading multiple libraries within the same session is not supported",name, oaAnalysisLoadedLibName)
    
    #load oaAnalysisUtils
    #if "OAANALYSISROOT" in os.environ:
    #    ROOT.gROOT.ProcessLine(".L ${OAANALYSISROOT}/src/ToaAnalysisUtils.cxx+")
    
    #Try to get the dataset from defined oaAnalysis datasets.
    try:
        inputDataset = datasets.DatasetDefs.getDataset(inputDataset.getName())
    except AttributeError:
        # No known matching dataset with this name, just use the file provided.
        pass

    inputFileName = inputDataset.getFirstFile()
    ROOT.TFile.SetReadStreamerInfo(False) #suppress missing dictionary warnings by not reading in streamer info.
    tfile = ROOT.TFile(inputFileName,"read")
    ROOT.TFile.SetReadStreamerInfo(True)
    if autoVersion:
        #Automatically determine the software version from header
        basicHeader = tfile.Get("HeaderDir/BasicHeader")
        if basicHeader and basicHeader.GetEntries() > 0:
            basicHeader.GetEntry(0)
            name = basicHeader.SoftwareVersion
            #remove null characters from SoftwareVersion
            name = "".join(name.split("\x00"))
            if ord(str(basicHeader.IsMC)): # IsMC is char type, so convert to integer with ord 
                name += "_mc"
            else:
                name += "_data"
        #No basic header
        else:
            name += "_unknown"
    outputPath = "libReadoaAnalysis"
    if len(name)>0:
        outputPath += "_"+name
    libraryFileName = outputPath+"/"+outputPath+".so"
#    libraryFileName = "./"+libraryFileName
    workingDir = os.getcwd()
    libraryFileName = workingDir+"/"+libraryFileName
    
    # get lock to revent multiple jobs simultaneously trying to make library
    lock = fileLock.FileLock(".lockFile_libReadoaAnalysis_"+name)    
    lock.lock()
    if not os.path.exists(libraryFileName):
        if not tfile.IsOpen():
            raise Exception("loadLibReadOaAnalysis could not open input file",inputFileName, name)
        tfile.ReadStreamerInfo() #manually read streamer info since it was disabled above
        tfile.MakeProject(outputPath,"*","update+")
    ROOT.gSystem.Load("libCint.so")
    ROOT.gSystem.Load(libraryFileName)
#    ROOT.gSystem.Load("libReadoaAnalysis/libReadoaAnalysis.so")
    hasLoadedoaAnalysisLib = True
    oaAnalysisLoadedLibName = name
    #load hack scripts
    oaAnalysisHacks.loadOaAnalysisHacks(outputPath, prod4=prod4)
    lock.release()
    return

