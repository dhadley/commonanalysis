"""Tools for parsing command line options.
"""

import optparse
import StringIO
import copy

###############################################################################

class OptionsParser:
    """Retrieve command line options.
    
    OptionsParser is a wrapper around optparse. It parses the command line for 
    standard unix options of the type -o or --example-option. 
    
    For example, 
    
    >>> import commandLine
    >>> parser = commandLine.OptionsParser()
    >>> #add option --fileName="user_filename"
    >>> parser.addValueOption("fileName","set file name", str, default="test.root")
    >>> #add a switch option --debug
    >>> parser.addSwitchOption("debug","switch on debug statements")
    >>> #add a choice of colors between red green and blue.
    >>> parser.addChoiceOption("color","pick a color",["red","green","blue"])
    >>> #once all options are added you need to call parse
    >>> parser.parse()
    >>>
    >>> #retrieve the values with the get method
    >>> fileName = parser.get("fileName")
    >>> debug = parser.get("debug")
    >>> color = parser.get("color")
    >>> print fileName,debug,color

    """
    def __init__(self):
        self.optParser = optparse.OptionParser()
        self.options = None
        self.args = None
        self.switchOptions = []
        self.choiceOptions = []
        self.valueOptions = []
    
    def addValueOption(self, name, description, dataType, short=None, default=None):
        """Add a option which takes a value as an argument e.g. --myOption="some_value".  
        
        :param name: the name used on the command line and to retrieve getOption function.
        :type name: str
        :param description: will appear in the help message i.e. when -h command line option is added.
        :type description: str
        :param dataType: specifies the expected type (e.g. str, int, float, bool).
        :type dataType: type
        :param short: provides a single character alias for this option e.g. if short=o then --myOption can also be called with the alias -o.
        :type short: str
        :param default: specifies a default value if the option is not set.
        """
        opts = ["--"+name]
        if short is not None:
            opts.append("-"+short)
        self.optParser.add_option(*opts, dest=name, type=dataType, default=default, help=description)
        self.valueOptions.append(name)
        return
    
    def addSwitchOption(self, name, description, short=None):
        '''Add a boolean switchable (True or False).   
        
        By default this value is off (False).
        Adding this option to the command line arguments switches the flag on (True).
        
        :param name: the name used on the command line and to retrieve get function.
        :param description: description will appear in the help message i.e. when -h command line option is added.
        :param short: short provides a single character alias for this option e.g. if short=o then --myOption can also be called with the alias -o.
        ''' 
        opts = ["--"+name]
        if short is not None:
            opts.append("-"+short)
        self.optParser.add_option(*opts, dest=name, action="store_true", default=False, help=description)
        self.switchOptions.append(name)
        return
        
    def addChoiceOption(self, name, description, choices, short=None, default=None):
        '''Add a option which takes 1 value from a predefined set of choices e.g. --myOption="some_value".
        
        If the value given on the command line is not in the choices then an exception is raised.
        
        :param name: the name used on the command line and to retrieve getOption function.
        :param description: description will appear in the help message i.e. when -h command line option is added.
        :param choices: is a list of possible options eg ["apples","bananas","pears"]
        :param short: provides a single character alias for this option e.g. if short=o then --myOption can also be called with the alias -o.
        :param default: specifies a default value if the option is not set.'''
        opts = ["--"+name]
        if short is not None:
            opts.append("-"+short)
        help = description + " (choose from: " + ",".join((str(c) for c in choices))+")"
        self.optParser.add_option(*opts, dest=name, default=default, help=help, choices=choices)
        self.choiceOptions.append(name)
        return
    
    def parse(self):
        """Parse command line options.
        
        Call once you have setup all of the options (eg with addSwitchOption, 
        addValueOption, addChoiceOption). This must be called before trying to 
        retrieve options with get.
        
        """
        if not self.options:
            options,args = self.optParser.parse_args()
            self.options = options
            self.args = args
        return 
        
    def getOptionsString(self):
        '''Returns the command-line option string that recreates this exact setup.
        
        This is useful when automatically generating batch submission scripts.
        
        :returns: a string that is equivalent to setting all options to their current values. 
        '''
        sio = StringIO.StringIO()
        #add switch options
        for name in self.switchOptions:
            if self.get(name):
                print >>sio,"--"+name,
        #add value options
        for name in self.valueOptions:
            value = self.get(name)
            if value is not None:
                print >>sio,"--"+name+"="+str(value),
        #add choice options
        for name in self.choiceOptions:
            value = self.get(name)
            if value is not None:
                print >>sio,"--"+name+"="+str(value),
        return sio.getvalue()
        
    def get(self, name):
        """Retrieve the value of a command line option.
        
        Note that you must have called parse before caling this method.
        
        :param name: name of the option.
        :type name: str
        :returns: current value of that option.
        """
        return getattr(self.options, name)
    
    def set(self, name, value, ignoreErrors=False):
        """Manually set the value of an option.
        
        This will override default or command line settings.
        
        :param name: of the option.
        :param value: the override value.
        """
        if ignoreErrors or hasattr(self.options,name):
             setattr(self.options,name,value)
        else:
            raise Exception("Error : attempted to manually set an invalid option", name, value)
        return 
    
    def clone(self):
        """Get a clone of the current state of this object.
        
        :returns: Return a deep copy of this object.
        """
        return copy.deepcopy(self)
        
###############################################################################

def _testOptionsParser():
    '''A very simple example of using the command line options module.'''
    options = OptionsParser()
    options.addValueOption("weight","set weight of fruit.", dataType=float, short="w", default=1.0)
    options.addChoiceOption("fruit","choose your fruit.", choices=["apples","bananas","pears"])
    options.addSwitchOption("imperial","use pounds instead of kilograms.", short="i")
    options.parse()
    
    #print out the command line options
    print "called python -m commonAnalysis.tools.commandLine",options.getOptionsString()
    weight = options.get("weight")
    fruit = options.get("fruit")
    useImperial = options.get("imperial")
    unit = "kg"
    if useImperial:
        unit = "pounds"
    print "You have selected",weight,unit,"of",fruit,"!"
    
    return

###############################################################################

def main():
    '''Runs a test of OptionsParser.'''
    _testOptionsParser()
    return

###############################################################################

if __name__ == "__main__":
    main()
        