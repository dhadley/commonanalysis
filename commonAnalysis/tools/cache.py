import hashlib
import os
import numpy

###############################################################################

class CacheNumpy:
    def __init__(self, uniquestr, prefix="tmp", tmpdir="/tmp/numpy_cache"):
        self._uniquestr = uniquestr
        self._tmpdir = tmpdir
        self._prefix = prefix
    
    def tmpfilename(self):
        uniquestr = self._uniquestr
        hashstr = hashlib.md5(uniquestr).hexdigest()
        tmpdir = self._tmpdir
        if not os.path.exists(tmpdir):
            os.makedirs(tmpdir)
        return "".join([tmpdir,
                        os.sep,
                        "_".join((self._prefix, hashstr)),
                        ".bin",
                        ])
    
    def exists(self):
        return os.path.exists(self.tmpfilename()) 
    
    def read(self):
        fname = self.tmpfilename()
        infile = file(fname, "rb")
        return numpy.load(infile)
    
    def write(self, data):
        fname = self.tmpfilename()
        outfile = file(fname, "wb")
        numpy.save(outfile, data)
        return

###############################################################################

def _test_numpy_cache():
    cn = CacheNumpy("_test_numpy_cache")
    if cn.exists():
        print "_test_numpy_cache exists, reading data"
        data = cn.read()
    else:
        print "_test_numpy_cache does not exist, writing data"
        data = numpy.ones(shape=(3, 2))
        cn.write(data)
    print data

###############################################################################

def main():
    _test_numpy_cache()
    return

if __name__ == "__main__":
    main()
