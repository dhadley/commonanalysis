#
# ROOT Leak finder helper script
# February 2008, Peter Waller <peter.waller@gmail.com>
#
# Usage:
#   from leakFinder import DeltaObjects
#
#   DeltaObjects( False ) # Initialises list of object allocations
#
#   # Do stuff here that leaks
#
#   DeltaObjects() # Prints list of objects derived from TObject that were 
#                  # allocated since the last call. Output looks something like
#                  # this:
#
#~ New objects:
#~ ----------------------------------------------------------
#~ |                     Class Type | Inst. |  Size | Delta |
#~ ----------------------------------------------------------
#~ |                      TRealData /   193 /    36 /     0 |
#~ |                  TStreamerBase /    11 /   128 /     0 |
#~ |          TStreamerBasicPointer /     2 /   144 /     0 |
#~ |             TStreamerBasicType /    30 /   124 /     0 |
#~ |                TStreamerObject /     4 /   120 /     0 |
#~ |             TStreamerObjectAny /     3 /   120 /     0 |
#~ |      TStreamerObjectAnyPointer /     2 /   120 /     0 |
#~ |         TStreamerObjectPointer /     1 /   120 /     0 |
#~ |                TStreamerString /     4 /   120 /     0 |
#~ ----------------------------------------------------------
#~ More objects:
#~ ----------------------------------------------------------
#~ |                     Class Type | Inst. |  Size | Delta |
#~ ----------------------------------------------------------
#~ |                          TAxis /     7 /   128 /     2 |
#~ |                     TBaseClass /   100 /    40 /    17 |
#~ |                         TClass /   128 /   192 /    16 |
#~ |                 TClassMenuItem /   158 /    60 /    15 |
#~ |                    TDataMember /  1024 /    88 /    74 |
#~ |                         TExMap /     5 /    24 /     2 |
#~ |                          TList /  3464 /    44 /   103 |
#~ |                        TMethod /  4925 /    76 /   132 |
#~ |                     TMethodArg /   203 /    40 /    18 |
#~ |                    TMethodCall /    14 /    60 /     6 |
#~ |                      TObjArray /   193 /    40 /    30 |
#~ |                        TObject /   297 /    12 /     6 |
#~ |                  TStreamerInfo /    15 /    96 /    13 |
#~ ----------------------------------------------------------
#~ --- Memory Delta: 29896 --- Object delta: 434

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ To obtain a copy of the license, please see <http://www.gnu.org/licenses/>.


###############################################################################

import ROOT

from pprint import pprint

oldObjects = {}
noChangeCount = 0
def DeltaObjects( displayChanges = True ):
    global oldObjects, noChangeCount
    
    ROOT.gObjectTable.UpdateInstCount()
    objects = map( lambda c: (c.GetName(), 
                             (c.GetInstanceCount(), 
                              c.GetHeapInstanceCount(), 
                              c.Size())), 
                   ROOT.gROOT.GetListOfClasses() )
    
    objects = dict ( objects )

    new = []
    increased = []
    decreased = []
    
    memDelta, objDelta = 0, 0
    
    for key, value in objects.iteritems():
        instances, heapinstances, size = value
        totInstances = instances + heapinstances
        
        if key not in oldObjects:
            if totInstances:
                new.append( (key, value, 0) )
        else:
            prevInstances, prevHeapInstances, dummy = oldObjects[key]
            
            totPrevInstances = prevInstances + prevHeapInstances
            
            #delta = totInstances - totPrevInstances
            delta = instances - prevInstances
            objDelta += delta
            
            memDelta += delta * size
            if delta < 0:
                decreased.append( (key, value, delta) )
            elif delta > 0:
                increased.append( (key, value, delta) )
            
        oldObjects[key] = value
            
    if displayChanges and objDelta:
        for l, name in [(new, "New"), (increased, "More"), (decreased, "Deleted")]:
            if not l: continue
                
            print name, "objects:"
            l.sort( key = lambda x: x[0] )
            #pprint ( l )
            spacings = ( 2 + 30 + 3 + 5 + 3 + 5 + 3 + 5 + 2 )
            print "-" * spacings 
            print "| %30s | %5s | %5s | %4s |" % ( "Class Type", "Inst.", "Size", "Delta" )
            print "-" * spacings
            for object, vals, delta in l:
                inst, heap, size = vals
                print "| %30s / %5i / %5i / %5i |" % (object, inst, size, delta )
            print "-" * spacings
            
        if noChangeCount:
            print noChangeCount, "calls since last change."
            noChangeCount = -1
        noChangeCount += 1
        print "--- Memory Delta:", memDelta, "--- Object delta:", objDelta
        
    elif displayChanges:
        noChangeCount += 1

###############################################################################

def main():
    print "-- leakFinder.py"
    # Initialise object counts
    DeltaObjects( False )
    h0 = ROOT.TH1F( "a00", "a", 100, 0, 100 )
    DeltaObjects()
    h = []
    for i in xrange( 10 ):
        h.append( ROOT.TH1F( "a%i" %i, "a", 100, 0, 100 ) )
        h.append(ROOT.TString("blah"))
    DeltaObjects()
    del h
    DeltaObjects()
    return


if __name__ == "__main__":
    main()
