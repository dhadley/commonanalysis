"""Make writing ntuples in pyroot easy.

For example, suppose that you want to write an ntuple consisting of two fields.
BranchPrimative is used for writing floating point numbers. 
BranchObject is used for writing C++ classes. 
First, particle mass (a floating point number) and 4-vector (TLorentzVector).

>>> import ntuple
>>> import ROOT
>>>
>>> #setup output file
>>> outputFileHandle = ROOT.TFile("outputFile.root","recreate")
>>> outputTree = ROOT.TTree("outputTree","an example ntuple")
>>> #setup branches
>>> branch_mass = ntuple.BranchPrimative("mass",outputTree)
>>> branch_fourMomentum = ntuple.BranchPrimative(TLorentzVector,"fourMomentum",outputTree)
>>> for i in xrange(1000):
>>>     #create some dummy data
>>>     mass = ROOT.gRandom.Gaus(91.,1.)
>>>     fourMom = ROOT.TLorentzVector(0.,0.,0.,mass)
>>>     #fill the output tree
>>>     branch_mass.setValue( mass )
>>>     branch_fourMom.setValue( fourMom )
>>>     outputTree.Fill()
>>> #write output
>>> outputTree.Write()
>>> outputFileHandle.Close()

"""
import itertools
import ROOT
import numpy

###############################################################################

class BranchPrimative:
    """Writes floating point numbers to ROOT TTree."""
    def __init__(self,name,tree, startValue=None):
        """
        :param name: name of the output variable.
        :type name: str
        :param tree: the output TTree.
        """
        self.name = name
        if startValue is None:
            startValue = 0.0
        self.value = numpy.array([startValue], dtype=float)
        self.startValue = startValue
        self.tree = tree
        self._setBranch()
        
    def _setBranch(self):
        branch = self.tree.FindBranch(self.name)
        if not branch:
            branch = self.tree.Branch(self.name, self.value, self.name+"/D" )
        else:
            #self.tree.SetBranchAddress(self.name, self.value, self.name+"/D" )
            self.tree.SetBranchAddress(self.name, self.value)
        self.branch = branch
    
    def _clearBranch(self):
        if self.branch:
            self.branch.SetAddress(0)
    
    def setValue(self,value):
        """Set the value. This should be done prior to calling TTree.Fill().
        
        :param value: value to be set for current entry in TTree.
        :type value: float 
        """
        self.value[0] = value
    
    def getValue(self):
        """
        :returns: current value.
        """
        return self.value[0]
    
    def reset(self):
        self.setValue(self.startValue)
    
    def __str__(self):
        return "(",self.name,self.value[0],")"
    
###############################################################################
    
class BranchObject:
    """Writes C++ objects to a ROOT TTree. 
    
    The object must have a default constructor that takes no arguments.
    The dictionaries for this object must be loaded.
    """
    def __init__(self,ObjectType, name, tree, startValue=None):
        """
        :param ObjectType: the class of this type eg ROOT.TLorentzVector
        :type ObjectType: class
        :param name: name of the output branch.
        :type name: str
        :param tree: the output TTree.
        :type tree: ROOT.TTree
        """
        self.name = name
        self.ObjectType = ObjectType
        if startValue is None:
            startValue = ObjectType() 
        self.value = startValue
        self.startValue = startValue
        self.tree = tree
        self._setBranch()
        
    def _setBranch(self):
        self.branch = self.tree.FindBranch(self.name)
        if not self.branch:
            self.branch = self.tree.Branch(self.name, self.value)
        else:
            #tree.SetBranchAddress(self.name, self.value)
            self.branch.SetAddress(ROOT.AddressOf(self.value))
    
    def _clearBranch(self):
        if self.branch:
            self.branch.SetAddress(0)

    def setValue(self,value):
        '''Set reference to the output object.
        
        Takes reference of the object to be writen. 
        Preferred method provided value is guaranteed to exist at time of ROOT.TTree.Fill.
        
        :param value: reference to the output object to be filled.
        '''
        self.value = value
        #update branch address
        self.branch.SetAddress(ROOT.AddressOf(self.value))
    
    def copyValue(self,value):
        '''Tries to copy the value. 
        
        Use in case value is not guaranteed to exist at the time of ROOT.TTree.Fill.
        This is potentially a slow method so setValue should be used if possible.
        
        :param value: reference to the output object to be copied then filled.
        '''
        self.value = self.ObjectType(value)
        #update branch address
        self.branch.SetAddress(ROOT.AddressOf(self.value))
        
    def reset(self):
        self.setValue(self.startValue)
    
    def getValue(self):
        """
        :returns: reference to the current object.
        """
        return self.value
    
    def __str__(self):
        return "BranchObject("+self.name+")"

###############################################################################

#old code, no longer used
#class BranchPrimativeDict:
#    def __init__(self,tree):
#        self.tree = tree
#        self.dic = dict()
#    def __getitem__(self,key):
#        if not key in self.dic:
#            self.dic[key] = BranchPrimative(key, self.tree)
#        return self.dic[key]
#    def fill(self):
#        self.tree.Fill()
#    #def __setitem__(self,key,value):
#    #    self.dic[key] = value
    
###############################################################################

class TreeCopier:
    
    def __init__(self,treeName,inputDataset,outputFileName):
        self.treeName = treeName
        self.inputDataset = inputDataset
        self.outputFileName = outputFileName
        #create input chain
        self.inputChain = ROOT.TChain(self.treeName,self.treeName)
        for fileName in self.inputDataset:
            self.inputChain.AddFile(fileName)
        #create output file
        self.outputFile = ROOT.TFile(self.outputFileName,"recreate")
        assert self.outputFile.IsOpen()
        self.outputTree = self.inputChain.CloneTree(0)
        self.outputFile.cd()
        
    def __iter__(self):
        for event in self.inputChain:
            yield event
            
    def getOutputTree(self):
        return self.outputTree

    def fill(self):
        self.outputTree.Fill()
    
    def close(self):
        self.outputTree.AutoSave()
        self.outputFile.Close()
        
    def numEntries(self):
        return self.inputChain.GetEntries()

###############################################################################

def _unitTestBranchObjWrite():
    print "--- _unitTestBranchObjWrite"
    #generate dictionaries
    ROOT.gInterpreter.GenerateDictionary("std::vector<TLorentzVector>","TLorentzVector.h")
    #create output tree
    tfile = ROOT.TFile("unitTestBranchObj.root","recreate")
    tree = ROOT.TTree("unitTestBranchObj","unitTestBranchObj")
    #define branches
    testDouble = BranchPrimative("testDouble", tree)
    testLorentzVec = BranchObj(ROOT.TLorentzVector,"testLorentzVec", tree)
    testStdVec = BranchObj(ROOT.std.vector("double"),"testStdVec", tree)
    #fill tree with some random data
    StdVecHlv = ROOT.std.vector("TLorentzVector")
    testStdVecLorentz = BranchObj(StdVecHlv,"testStdVecLorentz", tree)
    rand = ROOT.TRandom3()
    print "unitTestBranchObj begining event loop"
    for i in xrange(1000):
        testDouble.setValue(rand.Gaus(1,1))
        e = rand.Gaus(1000,100)
        hlv = ROOT.TLorentzVector(1.0,2.0,3.0,4.0)
        testLorentzVec.setValue(hlv)
        vec = ROOT.std.vector("double")()
        for i in xrange(10):
            vec.push_back(rand.Gaus(-1,1))
        testStdVec.setValue(vec)
        vecHlv = StdVecHlv()
        hlv1 = ROOT.TLorentzVector(1.0,2.0,3.0,4.0)
        hlv2 = ROOT.TLorentzVector(5.0,6.0,7.0,8.0)
        vecHlv.push_back(hlv1)
        vecHlv.push_back(hlv2)
        vecHlv.push_back(ROOT.TLorentzVector(9.0,10.0,11.0,12.0))
        testStdVecLorentz.setValue(vecHlv)
        tree.Fill()
    tree.Write()
    tfile.Close()
    print "--- _unitTestBranchObjWrite... done." 
    return

##################################################

def _unitTestBranchObjRead():
    print "--- _unitTestBranchObjRead"
    tfile = ROOT.TFile("unitTestBranchObj.root","read")
    tree = tfile.Get("unitTestBranchObj")
    print tree
    for i,event in enumerate(tree):
        print "TEST double = ",event.testDouble
        event.testLorentzVec.Print()
        print "TEST testStdVec = ",event.testStdVec.size(),[ x for x in event.testStdVec ]
        print "TEST vec Lorentz Vec:",event.testStdVecLorentz.size(),"entries"
        for hlv in event.testStdVecLorentz:
            hlv.Print()
        if i>=2:
            break
    print "--- _unitTestBranchObjRead ..done"
    return

##################################################

def main():
    """Run some unit tests that check reading and writing of a ttree still works."""
    _unitTestBranchObjWrite()
    _unitTestBranchObjRead()
    return

if __name__ == "__main__":
    main()
    
