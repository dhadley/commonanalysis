#!/bin/bash
# Script to generate the sphinx documentation for this package.

PACKAGENAME=commonAnalysis

#go to the right directory
cd ${COMMONANALYSISROOT}/doc
#update documentation for any new classes, modules etc.
if [ -f Makefile ]
then
    sphinx-apidoc -o ./ ../${PACKAGENAME}
else 
    sphinx-apidoc -F -o ./ ../${PACKAGENAME}
fi
#make to documentation
make html
